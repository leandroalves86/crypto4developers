package mc2018.tlsissues;

import mc2018._utils.CertUtils;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.PolicyNode;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.*;

/* https://docs.oracle.com/javase/8/docs/technotes/guides/security/jsse/JSSERefGuide.html
 * When using raw SSLSocket and SSLEngine classes, you should always check the
 * peer's credentials before sending any data. The SSLSocket and SSLEngine 
 * classes do not automatically verify that the host name in a URL matches the
 * host name in the peer's credentials. An application could be exploited with 
 * URL spoofing if the host name is not verified.
 * 
 * Listagem 2.28. Validação da cadeia de certificação no SSL/TLS.
*/
public final class Code24_SSLClientOnlyCertPathValidation {

  public static void main(String[] args) throws Exception {
    SSLSocket s = null; boolean ok = true;
    try {
      SSLSocketFactory f=(SSLSocketFactory)SSLSocketFactory.getDefault();
      s = (SSLSocket) f.createSocket("www.google.com", 443);
      s.startHandshake();//all validations happen after handshake
      SSLSession session = s.getSession();
      Principal peerPrincipal = session.getPeerPrincipal();

      // Step 1. Obtain root certs and cert path to validate
      Certificate[] certs = session.getPeerCertificates();
      X509Certificate[] x509certs = new X509Certificate[certs.length-1];
      for (int i = 0; i < certs.length-1; i++) {
        x509certs[i] = (X509Certificate) certs[i];
      }
      X509Certificate anchor = (X509Certificate) certs[certs.length-1];
      
      List l = Arrays.asList(x509certs);
      CertificateFactory cf=CertificateFactory.getInstance("X.509","SUN");
      CertPath cp = cf.generateCertPath(l);

      // Step 2. Create a PKIXParameters with the trust anchors
      TrustAnchor ta = new TrustAnchor(anchor, null);
      PKIXParameters params = new PKIXParameters(Collections.singleton(ta));
      params.setRevocationEnabled(false);// DANGER: Revocation status ignored

      // Step 3. Use a CertPathValidator to validate the certificate path
      CertPathValidator cpv = CertPathValidator.getInstance("PKIX","SUN");
      
      // validate certification path with specified params
      PKIXCertPathValidatorResult cpvr
              = (PKIXCertPathValidatorResult) cpv.validate(cp, params);
      PolicyNode policyTree = cpvr.getPolicyTree();
      PublicKey subjectPK = cpvr.getPublicKey();
      System.out.println("Certificate Chain successfully validated");
      System.out.println(subjectPK);

    }catch(CertificateException|InvalidAlgorithmParameterException| 
           NoSuchAlgorithmException | CertPathValidatorException e) 
    { System.out.println(e); ok = false; }
    
    if (ok) {CertUtils.handleSocket(s);} 
    else    {System.out.println("Something wrong in cert validation.");}
  }

}
