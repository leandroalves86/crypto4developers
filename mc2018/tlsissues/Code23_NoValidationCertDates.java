//this is a good use
package mc2018.tlsissues;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import mc2018._utils.CertUtils;

// Listagem 2.27. Validação manual dos certificados intermediários.
public final class Code23_NoValidationCertDates {

  public static boolean validate(X509Certificate cert, X509Certificate ca,
                                 X500Principal issuer, X500Principal subj,
                                 Date date) {
    boolean ok = false;
    try {
      ok = false; cert.verify(ca.getPublicKey()); ok = true;
    } 
    catch (CertificateException ex) { ok = false; System.out.println(ex);} 
    catch (NoSuchAlgorithmException |InvalidKeyException 
          |NoSuchProviderException  |SignatureException ex)   
    { ok = false;System.out.println(ex);}

    if (ok){
      ok = false;
      if (cert.getIssuerX500Principal().equals(issuer)){ ok =true;} 
      else { ok =false; System.out.println("Issuer name mismatch");}
      
      if (ok && cert.getSubjectX500Principal().equals(subj)){ ok = true;} 
      else {ok =false; System.out.println("Subject name mismatch");}
    }
    return ok;
  }

  public static void main(String[] args) {
    Security.addProvider(new BouncyCastleProvider());
    try {
      KeyPair rkp = CertUtils.genRSAKeyPair();
      X509Certificate root = CertUtils.buildSelfSignedCert(rkp);

      KeyPair mkp = CertUtils.genRSAKeyPair();
      X509Certificate middle = CertUtils.buildMiddleCert(mkp.getPublic(), 
              "CN=Middle CA Certificate", rkp.getPrivate(), root);
      
      // generate end entity certificate
      KeyPair ekp = CertUtils.genRSAKeyPair();
      X509Certificate user = CertUtils.buildEndCert( ekp.getPublic(), 
              "CN=User Certificate", mkp.getPrivate(), middle);
    
      X500Principal issuer = new X500Principal("CN=Root Certificate");
      X500Principal subj1 = new X500Principal("CN=Middle CA Certificate");
      X500Principal subj2 = new X500Principal("CN=User Certificate");
      
      if (validate(middle,root,issuer,subj1,null)) 
        System.out.println("Middle certificate successfully validated");
      if (validate(user,middle,subj1,subj2,null)) 
        System.out.println("User Certificate successfully validated");
      
    } catch (Exception ex) {System.out.println(ex);}
  }
}
