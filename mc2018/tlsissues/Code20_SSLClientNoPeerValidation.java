package mc2018.tlsissues;

import mc2018._utils.CertUtils;
import java.security.Principal;
import java.security.cert.Certificate;
import javax.net.ssl.*;

/* https://docs.oracle.com/javase/8/docs/technotes/guides/security/jsse/JSSERefGuide.html
 * When using raw SSLSocket and SSLEngine classes, you should always check the
 * peer's credentials before sending any data. The SSLSocket and SSLEngine 
 * classes do not automatically verify that the host name in a URL matches the
 * host name in the peer's credentials. An application could be exploited with 
 * URL spoofing if the host name is not verified.
*
* Listagem 2.24. Sem validação de certificados SSL/TLS.
 */
public final class Code20_SSLClientNoPeerValidation {

  public static void main(String[] args) throws Exception {
    SSLSocket s = null;
    Boolean ok = true;
    try {
      SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
      s = (SSLSocket) f.createSocket("www.google.com", 443);
      s.startHandshake();
      // all validations happen after handshake
      SSLSession session = s.getSession();
      Principal peerPrincipal = session.getPeerPrincipal();
      
      System.out.println();
      System.out.println("Session infos");
      System.out.println("Protocol: " + session.getProtocol());
      System.out.println("Ciphersuite: " + session.getCipherSuite());
      System.out.println("Host name: " + session.getPeerHost());
      System.out.println("\n"+peerPrincipal);
      System.out.println("\nPeer certificates");
      Certificate[] peerCertificates = session.getPeerCertificates();
      for (Certificate c : peerCertificates) { System.out.println(c);}
    } catch (Exception e) { System.out.println(e); ok = false; }
    
    System.out.println();
    if (ok) {CertUtils.handleSocket(s);} 
    else    {System.out.println("Something wrong in cert validation.");}
  }

}
