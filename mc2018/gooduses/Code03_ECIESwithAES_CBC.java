package mc2018.gooduses;

import mc2018.misuses.*;
import mc2018._utils.U;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public final class Code03_ECIESwithAES_CBC {
  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider());// provedor BC
      byte[] ptAna = ("Teste do ECIES").getBytes();
      KeyPairGenerator g = KeyPairGenerator.getInstance("ECIES","BC");
      g.initialize(224); KeyPair kp = g.generateKeyPair();

      Cipher e = Cipher.getInstance("ECIESwithAES-CBC/NONE/NOPADDING","BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      Cipher d = Cipher.getInstance("ECIESwithAES-CBC/NONE/NOPADDING","BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate());

      U.println("Plaintext : " + U.b2x(ptAna));
      U.println("Ciphertext: " + U.b2x(e.doFinal(ptAna)));
      U.println("Ciphertext: " + U.b2x(e.doFinal(ptAna)));
      U.println("Plaintext : " + U.b2x(d.doFinal(e.doFinal(ptAna))));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException |
            InvalidKeyException | IllegalBlockSizeException |
            BadPaddingException | NoSuchProviderException e) 
    { System.out.println(e); }
  }
}
