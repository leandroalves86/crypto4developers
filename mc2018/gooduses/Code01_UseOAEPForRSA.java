package mc2018.gooduses;

import mc2018._utils.U;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public final class Code01_UseOAEPForRSA {

  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider()); // provedor BC
      byte[] pt = ("Randomized RSA").getBytes();

      KeyPairGenerator g = KeyPairGenerator.getInstance("RSA", "BC");
      g.initialize(2048); KeyPair kp = g.generateKeyPair();
      String RSA_OAEP = "RSA/None/OAEPWithSHA256AndMGF1Padding";
      Cipher e = Cipher.getInstance(RSA_OAEP, "BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      Cipher d = Cipher.getInstance(RSA_OAEP, "BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate());

      U.println("Plaintext: " + U.b2x(pt));
      U.println("Ciphertext: " + U.b2x(e.doFinal(pt)));
      U.println("Ciphertext: " + U.b2x(e.doFinal(pt)));
      U.println("Ciphertext: " + U.b2x(e.doFinal(pt)));
      U.println("Plaintext: " + U.b2x(d.doFinal(e.doFinal(pt))));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException |
            InvalidKeyException | IllegalBlockSizeException |
            BadPaddingException | NoSuchProviderException e) 
    { System.out.println(e); }
  }
}
