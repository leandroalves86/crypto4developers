package mc2018.gooduses;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PSSParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


public final class Code04_PSS_128bitsSign3072xSHA256 {

  public static void main(String[] args) throws Exception {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    KeyPairGenerator kg = KeyPairGenerator.getInstance("RSA", "BC");
    kg.initialize(3072, new SecureRandom());
    KeyPair kp = kg.generateKeyPair();
    
    Signature sig = Signature.getInstance("SHA256withRSAandMGF1", "BC");
    PSSParameterSpec spec = new PSSParameterSpec("SHA256", "MGF1", 
            MGF1ParameterSpec.SHA256, 20, 1);
    sig.setParameter(spec);

    byte[] m = "Testing good RSA PSS".getBytes();
    sig.initSign(kp.getPrivate(), new SecureRandom());
    sig.update(m);
    byte[] s = sig.sign();

    sig.initVerify(kp.getPublic());
    sig.update(m);
    if (sig.verify(s)) { System.out.println("Verification succeeded.");}
    else               { System.out.println("Verification failed.");   }
  }
}
