package mc2018.gooduses;

import mc2018._utils.U;
import javax.crypto.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import javax.crypto.spec.PSource;
import org.bouncycastle.jce.provider.*;

public final class Code02_OtherOAEP_2048x512 {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider());
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA","BC");
    kpg.initialize(2048); KeyPair kp = kpg.generateKeyPair(); 
    
    // configuracoes comuns para Ana e Bato
    MGF1ParameterSpec mgf1ps = MGF1ParameterSpec.SHA512;
    OAEPParameterSpec OAEPps = new OAEPParameterSpec("SHA512","MGF1",
            mgf1ps,PSource.PSpecified.DEFAULT);
    Cipher c = Cipher.getInstance("RSA/None/OAEPPadding", "BC");
    
    // Encriptacao pela Ana com a chabe publica de Beto
    Key pubk = kp.getPublic();
    c.init(Cipher.ENCRYPT_MODE, pubk, OAEPps);
    int maxLenB = (2048-2*512)/8-2;//tamanho max do texto claro (bytes) 
    byte[] pt = U.cancaoDoExilio.substring(0, maxLenB).getBytes();
    byte[] ct = c.doFinal(pt);
    
    Key privk = kp.getPrivate();//decriptacao com chave privada do Beto
    c.init(Cipher.DECRYPT_MODE, privk, OAEPps); //inicializa para decriptacao
    byte[] ptBeto = c.doFinal(ct); // Decriptando

    U.println("Chave pública: " + pubk);
    U.println("Chave privada: " + privk);

    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Texto claro  da Ana: " + U.b2s(pt));
    U.println("Criptograma (A-->B): " + U.b2x(ct) + ", bits " + ct.length * 8);
    U.println("Texto claro do Beto: " + new String(ptBeto));
  }

}
