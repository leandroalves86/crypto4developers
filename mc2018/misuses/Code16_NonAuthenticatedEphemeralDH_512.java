package mc2018.misuses;

import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.crypto.*;

// Listagem 2.20. DH não autenticado e chaves fracas.
public final class Code16_NonAuthenticatedEphemeralDH_512 {

  public static void main(String argv[]) {
    try {
      KeyPairGenerator aKPG = KeyPairGenerator.getInstance("DH", "SunJCE");
      aKPG.initialize(512);
      KeyPair aKP = aKPG.generateKeyPair();

      KeyAgreement aKA = KeyAgreement.getInstance("DH", "SunJCE");
      aKA.init(aKP.getPrivate());

      byte[] aPubKe = aKP.getPublic().getEncoded();

      KeyFactory bKF = KeyFactory.getInstance("DH", "SunJCE");
      X509EncodedKeySpec x509ks = new X509EncodedKeySpec(aPubKe);
      PublicKey aPubK = bKF.generatePublic(x509ks);

      KeyPairGenerator bKPG = KeyPairGenerator.getInstance("DH", "SunJCE");
      bKPG.initialize(512);
      KeyPair bKP = bKPG.generateKeyPair();

      KeyAgreement bKA = KeyAgreement.getInstance("DH", "SunJCE");
      bKA.init(bKP.getPrivate());

      byte[] bPubKe = bKP.getPublic().getEncoded();

      KeyFactory aKF = KeyFactory.getInstance("DH", "SunJCE");
      x509ks = new X509EncodedKeySpec(bPubKe);
      PublicKey bPubK = aKF.generatePublic(x509ks);
      aKA.doPhase(bPubK, true);
      byte[] aSecret = aKA.generateSecret();

      bKA.doPhase(aPubK, true);
      byte[] bSecret = bKA.generateSecret();

      if(!Arrays.equals(aSecret,bSecret))throw new Exception("Secrets differ");
    } catch (Exception e) {System.err.println("Error: " + e);System.exit(1);}
  }
}
