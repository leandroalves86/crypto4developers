package mc2018.misuses;

import mc2018._utils.U;
import javax.crypto.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

// Listagem 2.12. RSA-OAEP com chave de 1024 bits e SHA-1.
// Encriptação e decriptação com chave assimétrica
public final class Code10_ImproperConfigRSA_1024x160 {

  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider());
      KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
      kpg.initialize(1024); KeyPair kp = kpg.generateKeyPair();

      Cipher c=Cipher.getInstance("RSA/None/OAEPwithSHA1andMGF1Padding");

      c.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      byte[] ptAna = U.cancaoDoExilio.substring(0,86).getBytes();
      byte[] ct = c.doFinal(ptAna);

      c.init(Cipher.DECRYPT_MODE, kp.getPrivate());
      byte[] ptBeto = c.doFinal(ct); // Decriptando

      U.println("Encriptado com: " + c.getAlgorithm());
      U.println("Texto claro  da Ana: " + U.b2s(ptAna));
      U.println("Criptograma (A-->B): " + U.b2x(ct)+", bits "+ct.length*8);
      U.println("Texto claro do Beto: " + new String(ptBeto));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException |
            InvalidKeyException | BadPaddingException |
            IllegalBlockSizeException | NoSuchProviderException e) 
    {System.out.println(e);}
  }
}
