package mc2018.misuses;

import mc2018._utils.U;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//Listagem 2.9. RSA canônico sem padding.
public final class Code08_InsecurePaddingRSA {

  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider()); // provedor BC
      byte[] pt = ("Cripto deterministica").getBytes();
      KeyPairGenerator g = KeyPairGenerator.getInstance("RSA","BC");
      g.initialize(2048); KeyPair kp = g.generateKeyPair();

      Cipher e = Cipher.getInstance("RSA/None/NoPadding","BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      Cipher d = Cipher.getInstance("RSA/None/NoPadding","BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate());
      U.println("Texto claro: " + U.b2x(pt));
      U.println("Encriptado com: " + e.getAlgorithm());
      U.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      U.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      U.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      U.println("Texto claro: " + U.b2x(d.doFinal(e.doFinal(pt))));
    } catch (NoSuchAlgorithmException|NoSuchPaddingException|
            InvalidKeyException|IllegalBlockSizeException |
            BadPaddingException|NoSuchProviderException e) 
    { System.out.println(e);}
  }
}
