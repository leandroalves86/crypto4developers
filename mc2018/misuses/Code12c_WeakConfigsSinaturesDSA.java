package mc2018.misuses;

import mc2018._utils.U;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.Signature;

// Listagem 2.16. Duas configurações inseguras de assinaturas digitais DSA.
public final class Code12c_WeakConfigsSinaturesDSA {

  public static void main(String[] args) throws Exception {
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA", "SUN");
    kpg.initialize(2048, SecureRandom.getInstanceStrong());
    KeyPair kp = kpg.generateKeyPair();
    
    String[] weakdsa = {"SHA1withDSA","NONEwithDSA"};
    byte[] doc = U.cancaoDoExilio.getBytes();

    for (String dsa : weakdsa) {
      Signature signer = Signature.getInstance(dsa, "SUN");
      signer.initSign(kp.getPrivate(), new SecureRandom());
      if (dsa.equals("NONEwithDSA")){
        MessageDigest md = MessageDigest.getInstance("SHA1");
        doc = md.digest(doc);//Data for RawDSA must be exactly 20 bytes long 
      } 
      signer.update(doc); byte[] sign = signer.sign();

      Signature verifier = Signature.getInstance(dsa, "SUN");
      verifier.initVerify(kp.getPublic()); verifier.update(doc);

      if (verifier.verify(sign)){U.println("Signature OK!");} 
      else                      {U.println("Signature not OK!");}
    }

  }
}
