package mc2018.misuses;

import mc2018._utils.U;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;

//Listagem 2.17. DH não autenticado e com parâmetros estáticos.
public final class Code27_KeyAgreementCurve25519 {

    public static void main(String argv[]) {
        try {
            AlgorithmParameterGenerator apg 
                    = AlgorithmParameterGenerator.getInstance("DH","SunJCE");
            apg.init(1024); // here is the size
            AlgorithmParameters p = apg.generateParameters();
            DHParameterSpec dhps
                  = (DHParameterSpec) p.getParameterSpec(DHParameterSpec.class);
            U.println("DH parameters "+p.getAlgorithm());
            U.println("DH parameters "+p.toString());
            
            //Alice creates her own DH key pair using the DH parameters from p
            U.println("ALICE: Generate DH keypair ...");
            KeyPairGenerator aKPG = KeyPairGenerator.getInstance("DH","SunJCE");
            aKPG.initialize(dhps);
            KeyPair aKP = aKPG.generateKeyPair();

            // Alice creates and initializes her DH KeyAgreement object
            U.println("ALICE: Initialization ...");
            KeyAgreement aKA = KeyAgreement.getInstance("DH","SunJCE");
            aKA.init(aKP.getPrivate());

            // Alice encodes her public key, and sends it over to Bob.
            byte[] aPubKe = aKP.getPublic().getEncoded();

            /* Let's turn over to Bob.
             * Bob has received Alice's public key in encoded format.
             * He instantiates a DH public key from the encoded key
             * material. */
            KeyFactory bKF = KeyFactory.getInstance("DH","SunJCE");
            X509EncodedKeySpec x509ks = new X509EncodedKeySpec(aPubKe);
            PublicKey aPubK = bKF.generatePublic(x509ks);

            /* Bob gets the DH parameters associated with Alice's
             * public key.
             * He must use the same parameters when he generates
             * his own key pair. */
            DHParameterSpec dhps2 = ((DHPublicKey) aPubK).getParams();

            // Bob creates his own DH key pair
            System.out.println("BOB: Generate DH keypair ...");
            KeyPairGenerator bKPG = KeyPairGenerator.getInstance("DH","SunJCE");
            bKPG.initialize(dhps2);
            KeyPair bKP = bKPG.generateKeyPair();

            // Bob creates and initializes his DH KeyAgreement object
            System.out.println("BOB: Initialization ...");
            KeyAgreement bKA = KeyAgreement.getInstance("DH","SunJCE");
            bKA.init(bKP.getPrivate());

            // Bob encodes his public key, and sends it over to Alice.
            byte[] bPubKe = bKP.getPublic().getEncoded();

            /* Alice uses Bob's public key for the first (and only)
             * phase of her version of the DH protocol.
             * Before she can do so, she has to instantiate a DH
             * public key from Bob's encoded key material. */
            KeyFactory aKF = KeyFactory.getInstance("DH","SunJCE");
            x509ks = new X509EncodedKeySpec(bPubKe);
            PublicKey bPubK = aKF.generatePublic(x509ks);
            System.out.println("ALICE: Execute PHASE1 ...");
            aKA.doPhase(bPubK, true);
            byte[] aSecret = aKA.generateSecret();


            /* Bob uses Alice's public key for the first (and only)
             * phase of his version of the DH protocol. */
            U.println("BOB: Execute PHASE1 ...");
            bKA.doPhase(aPubK, true);
            byte[] bSecret = bKA.generateSecret();

            /* At this stage, both Alice and Bob have completed
             * the DH key agreement protocol.
             * Both generate the (same) shared secret. */
            //System.out.println("Alice secret: " + U.b2x(aSecret));
            //System.out.println("Bob   secret: " + U.b2x(bSecret));
            
            if (!Arrays.equals(aSecret,bSecret)){throw new Exception("Secrets differ");}
            System.out.println("Shared secrets are the same and are ");
        }catch (Exception e){System.err.println("Error:" + e);System.exit(1);}
    }
}
