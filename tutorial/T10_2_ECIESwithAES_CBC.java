package tutorial;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public final class T10_2_ECIESwithAES_CBC {
  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider());
      byte[] ptAna = U.cancaoDoExilio.getBytes();
      KeyPairGenerator g = KeyPairGenerator.getInstance("ECIES","BC");
      g.initialize(224); 
      KeyPair kp = g.generateKeyPair();
      SecureRandom sr = SecureRandom.getInstanceStrong();
      Cipher e = Cipher.getInstance("ECIESwithAES-CBC/NONE/NOPADDING","BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic(), sr);
      Cipher d = Cipher.getInstance("ECIESwithAES-CBC/NONE/NOPADDING","BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate(), e.getParameters());

      System.out.println("Texto claro:" + U.b2x(ptAna));
      System.out.println("Criptograma:" + U.b2x(e.doFinal(ptAna)));
      System.out.println("Criptograma:" + U.b2x(e.doFinal(ptAna)));
      System.out.println("Texto claro:" + U.b2x(d.doFinal(e.doFinal(ptAna))));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException
            |InvalidKeyException      | IllegalBlockSizeException
            |BadPaddingException      | NoSuchProviderException
            |InvalidAlgorithmParameterException e) 
    { System.out.println(e); }
  }
}
