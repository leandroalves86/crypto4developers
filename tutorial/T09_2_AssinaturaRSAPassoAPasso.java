package tutorial;

import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class T09_2_AssinaturaRSAPassoAPasso {
  public static void main(String args[]) {
    byte[] mensagem = ("Esta mensagem será assinada").getBytes();
      try {
         KeyPairGenerator g = KeyPairGenerator.getInstance("RSA");
         g.initialize(2048);
         KeyPair kp = g.generateKeyPair();
         MessageDigest md = MessageDigest.getInstance("SHA1");
         
         md.update(mensagem);
         byte[] hashGerado = md.digest();
         Cipher c = Cipher.getInstance("RSA");
         c.init(Cipher.ENCRYPT_MODE, kp.getPrivate());
         byte[] hashAssinado = c.doFinal(hashGerado);
         System.out.println("Assinatura "+U.b2x(hashAssinado));
         md.update(mensagem);
         hashGerado = md.digest();
         hashAssinado = c.doFinal(hashGerado);
         System.out.println("Assinatura "+U.b2x(hashAssinado));

         byte[] hashCalculado = md.digest(mensagem);
         c.init(Cipher.DECRYPT_MODE, kp.getPublic());
         byte[] hashOriginal = c.doFinal(hashAssinado);
         if (Arrays.equals(hashOriginal, hashCalculado)) {
             System.out.println ("Assinatura confere.");
         } else { System.out.println ("Assinatura NÃO confere.");}
      } catch  (NoSuchAlgorithmException | NoSuchPaddingException 
               |InvalidKeyException      | IllegalBlockSizeException 
               | BadPaddingException e) { System.out.println(e); }
 }
}
