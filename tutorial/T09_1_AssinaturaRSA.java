package tutorial;

import java.security.*;

public class T09_1_AssinaturaRSA {

    public static void main(String args[]) {
        byte[] mensagem = ("Esta mensagem será assinada").getBytes();
        try {
            KeyPairGenerator g = KeyPairGenerator.getInstance("RSA");
            g.initialize(2048);
            KeyPair kp = g.generateKeyPair();

            Signature sig = Signature.getInstance("SHA1WithRSA");
            sig.initSign(kp.getPrivate());
            sig.update(mensagem);
            byte[] assinatura = sig.sign();
            System.out.println("Assinatura "+U.b2x(assinatura));
            sig.update(mensagem);
            assinatura = sig.sign();
            System.out.println("Assinatura "+U.b2x(assinatura));
            
            sig.initVerify(kp.getPublic());
            sig.update(mensagem);
            if (sig.verify(assinatura)) {
                System.out.println("Assinatura confere.");
            } else { System.out.println("Assinatura invalida.");}
            
        } catch (NoSuchAlgorithmException | InvalidKeyException 
                | SignatureException e) { System.out.println(e);}
    }
}
