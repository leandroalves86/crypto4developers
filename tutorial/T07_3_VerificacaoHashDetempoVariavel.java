package tutorial;

import java.security.*;
import org.bouncycastle.jce.provider.*;

public class T07_3_VerificacaoHashDetempoVariavel {

  public static void main(String args[]) {
   try {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    MessageDigest md = MessageDigest.getInstance("SHA512", "BC");
    boolean ok;
    long t1, t2;
    String method[] = {"isEqVa","MDisEq","Econst","BC.equ","BC.cte"};
    long ttx[][] = new long[5][10];
    //long tt1[] = new long[64], tt2[] = new long[64], tt3[] = new long[64];
    //long tt4[] = new long[64], tt5[] = new long[64];
    
    for (int x = 0; x < ttx.length; x++) {
      md.reset();
      byte[] hash1 = md.digest(U.cancaoDoExilio.getBytes());
      for (int i = 0; i < ttx[x].length; i++) { // 64 bytes
        md.reset();
        byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
        hash2[i] = (byte) (hash2[i] ^ 0x01);
        t1 = System.nanoTime();
        ok = ehIgualVar(hash2, hash1);
        t2 = System.nanoTime();
        ttx[x][i] = t2 - t1;
      }
    }
    
    System.out. printf("%3s; %6s, %6s, %6s, %6s, %6s\n", "Idx",
    method[0],method[1],method[2],method[3],method[4]);
    for (int i = 0; i < ttx[0].length; i++) {
      System.out. printf("%3d; %6d, %6d, %6d, %6d, %6d\n"
          ,i,ttx[0][i],ttx[1][i],ttx[2][i],ttx[3][i],ttx[4][i]);
    }
    /*
    md.reset();
    byte[] hash1 = md.digest(U.cancaoDoExilio.getBytes());
    for (int i = 0; i < tt1.length; i++) { // 64 bytes
     md.reset();
     byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
     hash2[i] = (byte) (hash2[i] ^ 0x01);
     t1 = System.nanoTime();
     ok = ehIgualVar(hash2, hash1);
     t2 = System.nanoTime();
     tt1[i] = t2 - t1;
    }
    method[0] = "isEqVa";

    for (int i = 0; i < tt1.length; i++) { // 64 bytes
     md.reset();
     byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
     hash2[i] = (byte) (hash2[i] ^ 0x01);
     t1 = System.nanoTime();
     ok = MessageDigest.isEqual(hash2, hash1);
     t2 = System.nanoTime();
     tt2[i] = t2 - t1;
    }
    method[1] = "MDisEq";

    for (int i = 0; i < tt1.length; i++) { // 64 bytes
     md.reset();
     byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
     hash2[i] = (byte) (hash2[i] ^ 0x01);
     t1 = System.nanoTime();
     ok = ehIgualConst(hash2, hash1);
     t2 = System.nanoTime();
     tt3[i] = t2 - t1;
    }
    method[2] = "Econst";

    for (int i = 0; i < tt1.length; i++) { // 64 bytes
     md.reset();
     byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
     hash2[i] = (byte) (hash2[i] ^ 0x01);
     t1 = System.nanoTime();
     ok = org.bouncycastle.util.Arrays.areEqual(hash2, hash1);
     t2 = System.nanoTime();
     tt4[i] = t2 - t1;
    }
    method[3] = "BC.equ";

    for (int i = 0; i < tt1.length; i++) { // 64 bytes
      md.reset();
      byte[] hash2 = md.digest(U.cancaoDoExilio.getBytes());
      hash2[i] = (byte) (hash2[i] ^ 0x01);
      t1 = System.nanoTime();
      ok = org.bouncycastle.util.Arrays.constantTimeAreEqual(hash2, hash1);
      t2 = System.nanoTime();
      tt5[i] = t2 - t1;
    }
    method[4] = "BC.cte";
        
    System.out. printf("%3s; %6s, %6s, %6s, %6s, %6s\n", "Idx",
    method[0],method[1],method[2],method[3],method[4]);
    for (int i = 0; i < tt1.length; i++) {
      System.out. printf("%3d; %6d, %6d, %6d, %6d, %6d\n"
                         ,i,tt1[i],tt2[i],tt3[i],tt4[i],tt5[i]);
    }
*/
   } catch(NoSuchAlgorithmException | NoSuchProviderException e) {}
  }

  static boolean ehIgualVar(byte[] a, byte[] b) {
    boolean igual = true;
    if (a.length != b.length) { igual = false; } 
    else {
      for (int i = 0; i < a.length; i++) {
        if (a[i] != b[i]) { igual = false; break; }
      }
    }
    return igual;
  }

  static boolean ehIgualConst(byte[] a, byte[] b) {
    boolean igual = true;
    if (a.length != b.length) { igual = false; } 
    else { 
      for (int i = 0; i < a.length; i++) {
        if (a[i] != b[i]) { igual = false; }
      }
    }
    return igual;
  }
}
