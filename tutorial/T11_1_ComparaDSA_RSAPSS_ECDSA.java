package tutorial;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class T11_1_ComparaDSA_RSAPSS_ECDSA {
  public static void main(String[] args) throws Exception {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    String esquema = "RSA";
    //String esquema = "DSA";
    //String esquema = "ECDSA";

    Signature signerAna = null, verifierBeto = null;
    KeyPairGenerator kpg = KeyPairGenerator.getInstance(esquema, "BC");

    // par de chaves de Ana e configurações do criptosistema
    switch (esquema) {
      case "RSA":
        kpg.initialize(2048, new SecureRandom());
        signerAna = Signature.getInstance("SHA256withRSAandMGF1", "BC");
        break;
      case "ECDSA":
        ECGenParameterSpec ec = new ECGenParameterSpec("prime256v1");
        kpg.initialize(ec, new SecureRandom());
        signerAna = Signature.getInstance("SHA256WithECDSA", "BC");
        break;
      case "DSA":
        kpg = KeyPairGenerator.getInstance("DSA", "BC");
        kpg.initialize(2048, new SecureRandom());
        signerAna = Signature.getInstance("SHA256WithDSA", "BC");
        break;
    }
    KeyPair kpAna = kpg.generateKeyPair();

    //Ana assina o documento
    signerAna.initSign(kpAna.getPrivate(), new SecureRandom());
    byte[] documento = U.cancaoDoExilio.getBytes();
    signerAna.update(documento);
    byte[] assinatura = signerAna.sign();

    switch (esquema) {
      case "RSA":
        verifierBeto = Signature.getInstance("SHA256withRSAandMGF1", "BC");
        break;
      case "DSA":
        verifierBeto = Signature.getInstance("SHA256WithDSA", "BC");
        break;
      case "ECDSA":
        verifierBeto = Signature.getInstance("SHA256WithECDSA", "BC");
        break;
    }

    //Beto verifica a assinatura
    verifierBeto.initVerify(kpAna.getPublic());
    verifierBeto.update(documento);
    boolean ok = verifierBeto.verify(assinatura);

    if (ok) { System.out.println("Assinatura verificada!");} 
    else    { System.out.println("Assinatura   inválida!");}

    System.out.println("Chave pública " + kpAna.getPublic());
    System.out.println("Chave privada " + kpAna.getPrivate());
    System.out.println("Assinado com: " + signerAna.getAlgorithm());
    System.out.println("Tamanho da assinatura: "+assinatura.length+" bytes");
    System.out.println("Assinatura  : " + U.b2x(assinatura));
  }
}
