package tutorial;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

class T06_1_ModosDeOperacao {
 public static void main(String[] a) {
   try {
     Security.addProvider(new BouncyCastleProvider()); // provedor BC
     boolean erroNoByte = false, trocaBloco = false, perdeBloco = true;
     if (erroNoByte) {System.out.println("Erro no bit 1 do 1o. bloco");}
     if (trocaBloco) {System.out.println("Troca 1o. e 2o. blocos");}
     if (perdeBloco) {System.out.println("Perde 1o. bloco");}
     
     byte[]  k = {0x01,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
                  0x08,0x09,0x0A,0x0B,0xC,0x0D,0x0E,0x0F};
     byte[] t = ("CriptografiaAES.CriptografiaAES.CriptografiaAES.").getBytes();
     System.out.println("Texto Ana : " + U.b2s(t));
     
     SecretKeySpec ks = new SecretKeySpec(k, "AES");
     String[] m = {"AES/ECB/NoPadding","AES/CBC/NoPadding","AES/CFB/NoPadding",
                   "AES/OFB/NoPadding","AES/CTR/NoPadding"};
     
     for (int i = 0; i < m.length; i++) {
       Cipher c = Cipher.getInstance(m[i], "BC");
       c.init(Cipher.ENCRYPT_MODE, ks);
       byte[] criptograma = c.doFinal(t);

       if (erroNoByte) {criptograma = erroNoByte(criptograma);}
       if (trocaBloco) {criptograma = trocaBloco(criptograma);}
       if (perdeBloco) {criptograma = perdeBloco(criptograma);}
       
       if(i != 0){c.init(Cipher.DECRYPT_MODE,ks,new IvParameterSpec(c.getIV()));
       } else    {c.init(Cipher.DECRYPT_MODE,ks);}
       byte[] textoBeto = c.doFinal(criptograma);
       System.out.println(m[i]);
       System.out.println("Texto Beto: " + U.b2s(textoBeto));
    }
   } catch (NoSuchAlgorithmException | NoSuchProviderException | 
            NoSuchPaddingException | InvalidKeyException | 
            IllegalBlockSizeException | BadPaddingException | 
            InvalidAlgorithmParameterException e) { System.out.println(e);}
 }

public static byte[] erroNoByte(byte[] a){
    a[1] = (byte)(a[1]^(byte)0x01);
    return a;
} 

public static byte[] perdeBloco(byte[] a){
  for (int j = 0; j < 16; j++) {
   byte temp = a[j]; a[j] = a[j+16]; a[j+16] = temp;
  }
  return a;
}

public static byte[] trocaBloco(byte[] a){
    byte[] temp = new byte[a.length - 16];
    for (int j = 0; j < temp.length; j++){temp[j] = a[j+16];}
    a = temp;
    return a;
}
}