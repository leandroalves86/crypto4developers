package tutorial;

import java.security.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

class T07_1_Hashes {
 public static void main(String[] a) {
   try {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    Object[] hashes = Security.getAlgorithms("MessageDigest").toArray();
    MessageDigest md[] = new MessageDigest[hashes.length];
    System.out.println("Provedor:Algoritmo");
    for (int i = 0; i < md.length; i++) {
      md[i] = MessageDigest.getInstance(hashes[i].toString());
      System.out.println(md[i].getProvider()+": "+md[i].getAlgorithm());
    }
    String[] in = {"","abc","abcdefghijklmnopqrstuvwxyz"};
    for (int i = 0; i < md.length; i++) {
      md[i] = MessageDigest.getInstance(hashes[i].toString());
      System.out.println("Algoritmo: " + md[i].getAlgorithm());
      System.out.println("Tamanho (bytes):" + md[i].getDigestLength());
      for (int j = 0;j < 3; j++){
        System.out.print("Hash (\"" + in[j] + "\") = ");  
        md[i].update(in[j].getBytes());
        byte[] out = md[i].digest();
        System.out.println(U.b2x(out));
      }
      System.out.println();
    }
   } catch (Exception e) {System.out.println(e);}
 }
}
