package tutorial;


import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jce.provider.*;

public class T04_2_EncriptaDecriptaAES {

 public static void main(String args[]) {
  Security.addProvider(new BouncyCastleProvider()); // provedor BC
  try {
    // configurações do sistema criptográfico para Ana e Beto
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC"); //gerador de chaves
    g.init(256); //chave AES de 256 bits (compartilhada)
    Key k = g.generateKey(); // Ana cria uma chave pseudoaleatória
    Cipher c = Cipher.getInstance("AES/CTR/PKCS7Padding", "BC");//instancia AES
    
    // Encriptação pela Ana
    c.init(Cipher.ENCRYPT_MODE, k); //inicializa o AES para encriptacao
    byte[] textoclaroAna = "TUTORIALTUTORIAL".getBytes(); //com 128 bits
    byte[] criptograma = c.doFinal(textoclaroAna);
    
    // decriptação pelo Beto
    byte[] iv = c.getIV();
    c.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(iv));
    byte[] textoclaroBeto = c.doFinal(criptograma);
    System.out.println("Texto claro do Beto: " + new String(textoclaroBeto));
  } catch ( NoSuchAlgorithmException  | NoSuchProviderException 
          | NoSuchPaddingException    | InvalidKeyException 
          | IllegalBlockSizeException | BadPaddingException 
          | InvalidAlgorithmParameterException ex) { }
 }
}
