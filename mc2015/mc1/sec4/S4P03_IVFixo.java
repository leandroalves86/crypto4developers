/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

// 1.4.3. IVs fixos ou reutilizados
public class S4P03_IVFixo {
  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException, 
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    byte[] textoClaroAna = ("Teste de IV fixoTeste de IV fixo").getBytes();
    byte[] iv = U.x2b("0123456789ABCDEF0123456789ABCDEF");
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC");
    g.init(128);
    Key k = g.generateKey();
    String[]aes = {"AES/CBC/NoPadding", "AES/OFB/NoPadding",
      "AES/CFB/NoPadding","AES/CTR/NoPadding"};
    boolean ivFixo = true;
    U.println("Texto claro: " + new String(textoClaroAna));
    U.println("Chave      : " + U.b2x(k.getEncoded()));
    U.println("Iv fixo    : " + U.b2x(iv)+"\n");
    for (int a = 0; a < aes.length; a++) {
      Cipher enc = Cipher.getInstance(aes[a], "BC");
      Cipher dec = Cipher.getInstance(aes[a], "BC");
      U.println("Encriptado com: " + enc.getAlgorithm());
      byte[][] criptograma = new byte[2][];
      for (int i = 0; i < 2; i++) {
        enc.init(Cipher.ENCRYPT_MODE, k, new IvParameterSpec(iv));
        criptograma[i] = enc.doFinal(textoClaroAna);
        dec.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(iv));
        byte[] textoClaroBeto = dec.doFinal(criptograma[i]);
        U.println("Criptograma   : " + U.b2x(criptograma[i]));
        U.println("Texto claro   : " + new String(textoClaroBeto));
        if (!ivFixo) iv[iv.length-1] = (byte) (iv[iv.length-1]^0x01);
      }
      if (Arrays.equals(criptograma[0], criptograma[1])) {
        U.println("Iguais\n");
      } else {
        U.println("Diferentes\n");
      }
    }
  }
}
