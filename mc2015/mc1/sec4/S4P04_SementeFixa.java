/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//1.4.4. Sementes fixas ou reutilizadas para PRNGs
public class S4P04_SementeFixa {
  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException, 
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    byte[] textoClaroAna = ("PRNG seed fixa..PRNG seed fixa..").getBytes();
    byte[] seed = U.x2b("0123456789ABCDEF0123456789ABCDEF");
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC");
    g.init(128);
    Key k = g.generateKey();
    String[]aes = {"AES/CBC/NoPadding", "AES/OFB/NoPadding",
      "AES/CFB/NoPadding", "AES/CTR/NoPadding"};
    for (int a = 0; a < aes.length; a++) {
      Cipher enc = Cipher.getInstance(aes[a], "BC");
      Cipher dec = Cipher.getInstance(aes[a], "BC");
      U.println("Encriptado com: " + enc.getAlgorithm());
      byte[][] criptograma = new byte[2][];
      for (int i = 0; i < 2; i++) {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG","SUN");
        //SecureRandom sr = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
        sr.setSeed(seed);
        enc.init(Cipher.ENCRYPT_MODE, k, sr);
        criptograma[i] = enc.doFinal(textoClaroAna);
        dec.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(enc.getIV()));
        byte[] textoClaroBeto = dec.doFinal(criptograma[i]);
        U.println("Criptograma   : " + U.b2x(criptograma[i]));
        U.println("Texto claro   : " + new String(textoClaroBeto));
      }
      if (Arrays.equals(criptograma[0], criptograma[1])) {
        U.println("Iguais\n");
      } else {
        U.println("Diferentes\n");
      }
    }
  }
}
