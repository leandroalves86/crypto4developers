/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4.p11;

import mc2015.mc1.sec4.U;
/* de acordo com o exemplo do Katz
* ataca um criptograma formado por duas partes c0|c1,
* onde c1 contém bytes de padding e c0 é o iv
* modificação do byte c0[i] causa modificação em c1[i].
*/
public class Attacker {

  public static void main(String[] args) {
    byte[] criptograma = PaddingOracle.encripta();// obtem um criptograma e iv
    Attacker.attackOracle(PaddingOracle.iv, criptograma);
  }
  
  public static void attackOracle(byte[] c0, byte[] c1) {
    // descobrindo qual é do indice do primeiro valor do padding.
    int pi = findOutPaddingIndex(c0, c1);
    if (pi != -1) {// a atende aas premissas de padding, então segue em frente.
      byte[] decrypted = decryptBlock(pi, c0, c1);
      U.println("O texto claro é: "+new String(decrypted));
      U.println("Pronto, decriptou!!!");
    }
  }

  static int findOutPaddingIndex(byte[] c0, byte[] c1) {
    byte[] iv;
    int i;
    boolean ok = false;
    for (i = 0; i < c0.length; i++) {
      iv = c0.clone();
      iv[i] = 1; // um valor qualquer
      ok = PaddingOracle.oracle(iv, c1);
      if (!ok) {
        U.println("Erro de padding no byte[" + i
                + "], então valor do padding é " + (c0.length - i));
        break;
      }
    }
    if ((i == 0 && !ok) || (i == c0.length && ok)) {
      U.println("Bloco só com padding ou sem padding.");
      i = -1;
    }
    return i;
  }
  
  public static byte[] decryptBlock(int i, byte[] c0, byte[] c1) {
    byte[] decrypted = new byte[16];
    if (i > 0) {
      byte p = (byte) (c0.length - i); // salva o valor do padding
      byte[] iv = c0.clone();// cópia de trabalho do c0 em iv
      while (i > 0) {
        for (int j = i; j < iv.length; j++) {// preenche com p+1 de teste
          iv[j] = (byte) ((p + 1) ^ (byte) iv[j] ^ (p));
        }
        byte b; // descobre o b tal que b^(p+1) decripta correto para iv[i-1]
        for (b = 0; b < 256; b++) {
          iv[i - 1] = b;
          if (PaddingOracle.oracle(iv, c1)) {
            break;// descobriu valor de b
          }
        }
        // remove o padding de teste (p+1) e inclui o iv correto c0[i-1]
        decrypted[i - 1] = (byte) (c0[i - 1] ^ b ^ (p + 1));
        U.println("" + (char) ((byte) (c0[i - 1] ^ b ^ (p + 1))));
        U.delay(1);// atraso para facilitar a visualização
        i--;
        p = (byte) (p + 1);
      }
    }
    return decrypted;
  }
}
