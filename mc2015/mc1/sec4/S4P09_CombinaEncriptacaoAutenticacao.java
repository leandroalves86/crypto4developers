/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;

//1.3.4. Verificação de integridade e autenticação de mensagem
public class S4P09_CombinaEncriptacaoAutenticacao {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException, 
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    byte[] k =  U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
    byte[] iv = U.x2b("0123456789ABCDEF0123456789ABCDEF");
    SecretKeySpec sks1 = new SecretKeySpec(k, "AES");
    SecretKeySpec sks2 = new SecretKeySpec(k, "HMACSHA256");
    Cipher c = Cipher.getInstance("AES/CTR/NoPadding", "BC");
    Mac m = Mac.getInstance("HMACSHA256", "BC");
    byte[] textoclaroAna = "De Ana para Beto".getBytes(), X;
    boolean ok, ivo = true;

    // encripta entao autentica: Encrypt-then-MAC (EtM) 
    String s = "Encrypt-then-MAC(EtM): calcula a tag do criptograma";
    m.init(sks2); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    byte[] criptograma = c.doFinal(textoclaroAna);
    byte[] tag = m.doFinal(criptograma); // calcula a tag do criptograma

    if (ivo){
      X = U.xor("De Ana para Beto".getBytes(), "De Ana para  Ivo".getBytes());
      criptograma = U.xor(criptograma, X);
    }
    
    // decriptação pelo Beto com verificação da tag 
    m.init(sks2); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    byte[] textoclaroBeto = c.doFinal(criptograma); // decripta
    ok = MessageDigest.isEqual(m.doFinal(criptograma), tag); // verifica tag
    p(s,tag,textoclaroBeto,m,c,ok);
    
    // encripta e autentica: Encrypt-and-MAC (E&M)  - a tag é determinística
    s = "Encrypt-and-MAC (E&M): calcula a tag do texto claro";
    m.init(sks2); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    criptograma = c.doFinal(textoclaroAna); // encripta
    tag = m.doFinal(textoclaroAna); // calcula a tag do texto claro

    if (ivo){
      X = U.xor("De Ana para Beto".getBytes(), "De Ana para  Ivo".getBytes());
      criptograma = U.xor(criptograma, X);
    }
    
    // decriptação pelo Beto com verificação da tag 
    m.init(sks2); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    textoclaroBeto = c.doFinal(criptograma); // decripta
    ok = MessageDigest.isEqual(m.doFinal(textoclaroBeto), tag);// verifica tag
    p(s,tag,textoclaroBeto,m,c,ok);
    
    // autentica entao encripta: MAC-then-Encrypt (MtE) - a tag é deterministica
    s = "MAC-then-Encrypt (MtE): calcula a tag do texto claro";
    m.init(sks2); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    tag = m.doFinal(textoclaroAna);// calcula a tag do texto claro
    byte[] pacote = Arrays.concatenate(textoclaroAna,tag);
    criptograma = c.doFinal(pacote); // encrita texto e tag

    if (ivo){
      X = U.xor("De Ana para Beto".getBytes(), "De Ana para  Ivo".getBytes());
      byte[] cripTexto = Arrays.copyOfRange(criptograma,0,16);
      byte[] cripTag   = Arrays.copyOfRange(criptograma,16,criptograma.length);
      criptograma = Arrays.concatenate(U.xor(cripTexto, X),cripTag);
    }
    // decriptação pelo Beto com verificação da tag 
    m.init(sks2); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    pacote = c.doFinal(criptograma); // decript texto e tag
    textoclaroBeto = Arrays.copyOfRange(pacote, 0, 16);
    tag = Arrays.copyOfRange(pacote, 16, pacote.length);
    ok = MessageDigest.isEqual(m.doFinal(textoclaroBeto), tag); // verifica tag
    p(s,tag,textoclaroBeto,m,c,ok);
  }
  
  static void p(String s, byte[] t, byte[] tc, Mac m, Cipher c, boolean ok){
    U.println(s);
    U.println("Verificado com " + m.getAlgorithm() + ": " + ok);
    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Valor da   tag: " + U.b2x(t));
    U.println("Texto claro do Beto: " + new String(tc)+"\n");
  }
}
