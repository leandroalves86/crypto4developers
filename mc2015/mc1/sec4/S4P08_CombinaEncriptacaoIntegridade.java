/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;

//1.4.8. Combinação de integridade e encriptação
public class S4P08_CombinaEncriptacaoIntegridade {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException, 
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    byte[] k =  U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
    byte[] iv = U.x2b("0123456789ABCDEF0123456789ABCDEF");
    SecretKeySpec sks1 = new SecretKeySpec(k, "AES");
    Cipher c = Cipher.getInstance("AES/CTR/NoPadding", "BC");
    MessageDigest md = MessageDigest.getInstance("SHA256", "BC");
    byte[] textoclaroAna = "De Ana para Beto".getBytes(), X, Y;
    boolean ok, ivo = true;

    // encripta entao hash: Encrypt-then-Hash (EtH) 
    String s = "Encrypt-then-Hash(EtH): calcula o hash do criptograma";
    md.reset(); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    byte[] criptograma = c.doFinal(textoclaroAna);
    byte[] hash = md.digest(criptograma); // calcula a hash do criptograma

    if (ivo){
      X = U.xor("De Ana para Beto".getBytes(), "De Ana para  Ivo".getBytes());
      criptograma = U.xor(criptograma, X);
      hash = md.digest(criptograma);
    }
    
    // decriptação pelo Beto com verificação da hash 
    md.reset(); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    byte[] textoclaroBeto = c.doFinal(criptograma); // decripta
    ok = MessageDigest.isEqual(md.digest(criptograma), hash); // verifica hash
    p(s,hash,textoclaroBeto,md,c,ok);
    
    // encripta e hash: Encrypt-and-Hash (E&H) 
    s = "Encrypt-and-Hash (E&H): calcula o hash do texto claro";
    md.reset(); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    criptograma = c.doFinal(textoclaroAna); // encripta
    hash = md.digest(textoclaroAna); // calcula a hash do texto claro

    if (ivo){
      X = U.xor(textoclaroAna, "De Ana para  Ivo".getBytes());
      criptograma = U.xor(criptograma, X);
      hash = md.digest("De Ana para  Ivo".getBytes());
    }
    
    // decriptação pelo Beto com verificação da hash 
    md.reset(); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    textoclaroBeto = c.doFinal(criptograma); // decripta
    ok = MessageDigest.isEqual(md.digest(textoclaroBeto), hash);// verifica hash
    p(s,hash,textoclaroBeto,md,c,ok);
    
    // Hash entao encripta: Hash-then-Encrypt (HtE)
    s = "Hash-then-Encrypt(HtE):calcula o hash do texto claro e encripta a tag";
    md.reset(); c.init(Cipher.ENCRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    hash = md.digest(textoclaroAna);// calcula a hash do texto claro
    byte[] pacote = Arrays.concatenate(textoclaroAna,hash);
    criptograma = c.doFinal(pacote); // encrita texto e hash

    if (ivo){
      X = U.xor(textoclaroAna, "De Ana para  Ivo".getBytes());
      byte[] cripTexto = Arrays.copyOfRange(criptograma,0,16);
      byte[] cripTag   = Arrays.copyOfRange(criptograma,16,criptograma.length);
      md.reset(); byte[] t1 = md.digest("De Ana para Beto".getBytes());
      md.reset(); byte[] t2 = md.digest("De Ana para  Ivo".getBytes());
      Y = U.xor(t1, t2);
      criptograma = Arrays.concatenate(U.xor(cripTexto, X),U.xor(cripTag,Y));
    }
    // decriptação pelo Beto com verificação da hash 
    md.reset(); c.init(Cipher.DECRYPT_MODE, sks1, new IvParameterSpec(iv)); 
    pacote = c.doFinal(criptograma); // decript texto e hash
    textoclaroBeto = Arrays.copyOfRange(pacote, 0, 16);
    hash = Arrays.copyOfRange(pacote, 16, pacote.length);
    ok = MessageDigest.isEqual(md.digest(textoclaroBeto), hash); // verifica hash
    p(s,hash,textoclaroBeto,md,c,ok);
  }
  
  static void p(String s, byte[] t, byte[] tc, MessageDigest m, Cipher c, 
          boolean ok){
    U.println(s);
    U.println("Verificado com " + m.getAlgorithm() + ": " + ok);
    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Valor da   tag: " + U.b2x(t));
    U.println("Texto claro do Beto: " + new String(tc)+"\n");
  }
}
