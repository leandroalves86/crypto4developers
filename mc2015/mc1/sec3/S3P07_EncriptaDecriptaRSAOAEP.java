/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import javax.crypto.spec.PSource;
import org.bouncycastle.jce.provider.*;

// 1.3.7. Encriptação e decriptação com chave assimétrica
public class S3P07_EncriptaDecriptaRSAOAEP {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    int ksize = 2048; // tamanho da chave RSA
    int hsize = 256; // tamanho do hash 
    String rsaName = "RSA/None/OAEPwithSHA256andMGF1Padding";
    String hashName = "SHA-256";
    int maxLenBytes = (ksize - 2 * hsize) / 8 - 2; // tamanho máximo do texto claro 
    
    // Beto cria um par de chaves
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
    kpg.initialize(ksize);
    KeyPair kp = kpg.generateKeyPair(); 
    
    // configurações comuns para Ana e Bato
    MGF1ParameterSpec mgf1ps = MGF1ParameterSpec.SHA256;
    OAEPParameterSpec OAEPps = new OAEPParameterSpec(hashName, "MGF1",
            mgf1ps, PSource.PSpecified.DEFAULT);
    Cipher c = Cipher.getInstance(rsaName, "BC");

    
    
    // Encriptação pela Ana com a chabe pública de Beto
    Key pubk = kp.getPublic();
    c.init(Cipher.ENCRYPT_MODE, pubk, OAEPps);
    byte[] textoclaroAna = cancaoDoExilio.substring(0, maxLenBytes).getBytes();
    byte[] criptograma = c.doFinal(textoclaroAna);

    // decriptação pelo Beto com sua chave privada
    Key privk = kp.getPrivate();
    c.init(Cipher.DECRYPT_MODE, privk, OAEPps); //inicializa o AES para decriptacao
    byte[] textoclaroBeto = c.doFinal(criptograma); // Decriptando

    U.println("Chave pública: " + pubk);
    U.println("Chave privada: " + privk);

    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Texto claro  da Ana: " + U.b2s(textoclaroAna));
    U.println("Criptograma (A-->B): " + U.b2x(criptograma) + ", bits " + criptograma.length * 8);
    U.println("Texto claro do Beto: " + new String(textoclaroBeto));
  }

    //de Antônio Gonçalves Dias - versão sem acentos ...
  // deste modo, cada caracter é um byte e a conta do getBytes acerta...
  static String cancaoDoExilio = "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "As aves que aqui gorjeiam"
          + "Nao gorjeiam como lah."
          //+ ""
          + "Nosso ceu tem mais estrelas,"
          + "Nossas varzeas tem mais flores."
          + "Nossos bosques tem mais vida,"
          + "Nossa vida mais amores."
          //+ ""
          + "Em cismar, sozinho, aa noite,"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Minha terra tem primores"
          + "Que tais nao encontro eu cah;"
          + "Em cismar — sozinho, aa noite —"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Nao permita Deus que eu morra"
          + "Sem que eu volte para lah;"
          + "Sem que desfrute os primores"
          + "Que nao encontro por cah;"
          + "Sem que ainda aviste as palmeiras"
          + "Onde canta o sabiah.";
}

/*
 Chave (bits)	Hash(Bits)	TC max(bits)	TC max(bytes)
 384             160             48              6
 512             160             176             22
 768             160             432             54
 768             256             240             30
 1024            160             688             86
 1024            256             496             62
 1024            384             240             30
 2048            160             1712            214
 2048            256             1520            190
 2048            384             1264            158
 2048            512             1008            126
 3096            160             2760            345
 3096            256             2568            321
 3096            384             2312            289
 3096            512             2056            257
 */
