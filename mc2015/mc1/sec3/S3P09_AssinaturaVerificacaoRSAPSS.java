/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//1.3.9. Assinaturas digitais e verificação de autenticidade
public class S3P09_AssinaturaVerificacaoRSAPSS {

  public static void main(String[] args) throws Exception {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

        //String criptosis = "RSA";
    //String criptosis = "DSA";
    String criptosis = "ECDSA";

    Signature assinadorAna = null, verificadorBeto = null;
    KeyPairGenerator kpg = KeyPairGenerator.getInstance(criptosis, "BC");

    // par de chaves de Ana e configurações do criptosistema
    switch (criptosis) {
      case "RSA":
        kpg.initialize(3072, new SecureRandom());//3072 com SHA256
        assinadorAna = Signature.getInstance("SHA256withRSAandMGF1", "BC");
        break;
      case "ECDSA":
        //curva prima de 256|239|192 bits
        ECGenParameterSpec ec = new ECGenParameterSpec("prime256v1");
        kpg.initialize(ec, new SecureRandom());
        assinadorAna = Signature.getInstance("SHA256WithECDSA", "BC");
        break;
      case "DSA":
        kpg = KeyPairGenerator.getInstance("DSA", "BC");
        kpg.initialize(3072, new SecureRandom()); // 3072 com SHA256
        assinadorAna = Signature.getInstance("SHA256WithDSA", "BC");
        break;
    }

    KeyPair kpAna = kpg.generateKeyPair();

    //Ana assina o documento
    assinadorAna.initSign(kpAna.getPrivate(), new SecureRandom());
    byte[] documento = cancaoDoExilio.getBytes();
    assinadorAna.update(documento);
    byte[] assinatura = assinadorAna.sign();

        // problemas durante o transporte do documento
    // modifica documento
    //documento[0] = (byte)(documento[0]^(byte)0x01);
    // modifica assinatura
    //assinatura[0] = (byte)(assinatura[0]^(byte)0x01);
    // Beto configura seu criptosistema
    switch (criptosis) {
      case "RSA":
        verificadorBeto = Signature.getInstance("SHA256withRSAandMGF1", "BC");
        break;
      case "DSA":
        verificadorBeto = Signature.getInstance("SHA256WithDSA", "BC");
        break;
      case "ECDSA":
        verificadorBeto = Signature.getInstance("SHA256WithECDSA", "BC");
        break;
    }

    //Beto verifica a assinatura
    verificadorBeto.initVerify(kpAna.getPublic());
    verificadorBeto.update(documento);
    boolean ok = verificadorBeto.verify(assinatura);

    if (ok) {
      System.out.println("Assinatura verificada!");
    } else {
      System.out.println("Assinatura   inválida!");
    }

    U.println("Chave pública " + kpAna.getPublic());
    U.println("Chave privada " + kpAna.getPrivate());
    U.println("Assinado com: " + assinadorAna.getAlgorithm());
    U.println("Tamanho da assinatura: " + assinatura.length + " bytes");
    U.println("Assinatura  : " + U.b2x(assinatura));
  }

    //de Antônio Gonçalves Dias - versão sem acentos ...
  // deste modo, cada caracter é um byte. e a conta do getBytes acerta...
  static String cancaoDoExilio = "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "As aves que aqui gorjeiam"
          + "Nao gorjeiam como lah."
          + "Nosso ceu tem mais estrelas,"
          + "Nossas varzeas tem mais flores."
          + "Nossos bosques tem mais vida,"
          + "Nossa vida mais amores."
          + "Em cismar, sozinho, aa noite,"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "Minha terra tem primores"
          + "Que tais nao encontro eu cah;"
          + "Em cismar — sozinho, aa noite —"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "Nao permita Deus que eu morra"
          + "Sem que eu volte para lah;"
          + "Sem que desfrute os primores"
          + "Que nao encontro por cah;"
          + "Sem que ainda aviste as palmeiras"
          + "Onde canta o sabiah.";
}
