/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jce.provider.*;

// 1.3.2. Encriptação e decriptação com chave secreta
public class S3P01_EncriptaDecriptaAES {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC"); //gerador de chaves
    g.init(256); //chave AES de 128 bits (compartilhada)
    Key k = g.generateKey(); // Ana cria uma chave pseudoaleatória
    Cipher c = Cipher.getInstance("AES/CTR/PKCS7Padding", "BC");//instancia AES
    // Encriptação pela Ana
    c.init(Cipher.ENCRYPT_MODE, k); //inicializa o AES para encriptacao
    byte[] textoclaroAna = "Testando o AES..".getBytes(); //com 128 bits
    byte[] criptograma = c.doFinal(textoclaroAna);
    byte[] iv = c.getIV();
    // decriptação pelo Beto
    c.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(iv)); //inicializa o AES para decriptacao
    byte[] textoclaroBeto = c.doFinal(criptograma); // Decriptando

    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Texto claro  da Ana: " + U.b2s(textoclaroAna));
    U.println("Texto claro em hexa: " + U.b2x(textoclaroAna));
    U.println("Criptograma (A-->B): " + U.b2x(criptograma));
    U.println("Texto claro do Beto: " + new String(textoclaroBeto));
    U.println("Chave criptográfica: " + U.b2x(k.getEncoded()));
  }
}
