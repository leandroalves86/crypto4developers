/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//1.3.4. Verificação de integridade e autenticação de mensagem
public class S3P05_EncriptaEntaoMAC {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    byte[] k = U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
    SecretKeySpec sks1 = new SecretKeySpec(k, "AES");
    SecretKeySpec sks2 = new SecretKeySpec(k, "HMACSHA256");
    Cipher c = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
    Mac m = Mac.getInstance("HMACSHA256", "BC");

    // encriptação pela Ana com tag de autenticação da mensagem
    m.init(sks2); c.init(Cipher.ENCRYPT_MODE, sks1);
    byte[] textoclaroAna = cancaoDoExilio.getBytes();
    byte[] criptograma = c.doFinal(textoclaroAna);
    byte[] tag = m.doFinal(criptograma); // calcula a tag do criptograma

    // decriptação pelo Beto com verificação da tag 
    m.init(sks2); c.init(Cipher.DECRYPT_MODE, sks1); //inicializa o AES para decriptacao
    byte[] textoclaroBeto = c.doFinal(criptograma); // Decriptando
    // verifica a tag recebida com um valor calculado por Beto
    boolean ok = MessageDigest.isEqual(m.doFinal(criptograma), tag);
    U.println("Verificado com " + m.getAlgorithm() + ": " + ok);
    U.println("Encriptado com: " + c.getAlgorithm());
    U.println("Valor da   tag: " + U.b2x(tag));
    U.println("Texto claro do Beto:\n" + new String(textoclaroBeto));
  }
    //de Antônio Gonçalves Dias - versão sem acentos ...
  // deste modo, cada caracter é um byte. e aconta do getBytes acerta...
  static String cancaoDoExilio = "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "As aves que aqui gorjeiam"
          + "Nao gorjeiam como lah."
          //+ ""
          + "Nosso ceu tem mais estrelas,"
          + "Nossas varzeas tem mais flores."
          + "Nossos bosques tem mais vida,"
          + "Nossa vida mais amores."
          //+ ""
          + "Em cismar, sozinho, aa noite,"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Minha terra tem primores"
          + "Que tais nao encontro eu cah;"
          + "Em cismar — sozinho, aa noite —"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Nao permita Deus que eu morra"
          + "Sem que eu volte para lah;"
          + "Sem que desfrute os primores"
          + "Que nao encontro por cah;"
          + "Sem que ainda aviste as palmeiras"
          + "Onde canta o sabiah.";
}
