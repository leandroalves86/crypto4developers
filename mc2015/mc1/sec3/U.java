/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

public class U {
  public static void println(String s){System.out.println(s);}
  
  public static String b2s(byte [] ba){return new String(ba);}

  public static byte[] x2b(String str) {
           if (str == null) { return null;
    } else if (str.length() < 2) { return null;
    } else {
      int len = str.length() / 2;
      byte[] buffer = new byte[len];
      for (int i = 0; i < len; i++) {
        buffer[i] = (byte) Integer.parseInt(
                str.substring(i * 2, i * 2 + 2), 16);
      }
      return buffer;
    }
  }

  public static String b2x(byte[] data) {
    if (data == null) {
      return null;
    } else {
      int len = data.length;
      String str = "";
      for (int i = 0; i < len; i++) {
        if ((data[i] & 0xFF) < 16) {
          str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
        } else {
          str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
        }
      }
      return str.toUpperCase();
    }
  }
}
