/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

// 1.3.5. Encriptação autenticada
public class S3P06_EncriptacaoAutenticada {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    byte[] k = U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
    byte[] iv = U.x2b("ABCDEF1234567890ABCDEF1234567890");
    SecretKeySpec ks = new SecretKeySpec(k, "AES");
    GCMParameterSpec gps = new GCMParameterSpec(128, iv);//tag size + iv
    Cipher c = Cipher.getInstance("AES/GCM/NoPadding", "BC");

    // Encriptação pela Ana
    c.init(Cipher.ENCRYPT_MODE, ks, gps); //inicializa o AES para encriptacao
    byte[] textoclaroAna = "Testando o GCM..".getBytes();
    //byte[] textoclaroAna = cancaoDoExilio.getBytes();
    c.updateAAD("AAD nao estah cifrado...".getBytes());
    byte[] criptograma = c.doFinal(textoclaroAna);

        //criptograma[0] = (byte)(criptograma[0]^(byte)0x01);
    // decriptação pelo Beto
    c.init(Cipher.DECRYPT_MODE, ks, gps); //inicializa o AES para decriptacao
    c.updateAAD("AAD nao estah cifrado...".getBytes());
    boolean ok = true;
    byte[] textoclaroBeto = null;
    try {
      textoclaroBeto = c.doFinal(criptograma); // Decriptando
    } catch (AEADBadTagException e) {
      ok = false;
    }
    if (ok) {
      U.println("Encriptado   com: " + c.getAlgorithm());
      U.println("Usando a  tag de: " + gps.getTLen() + " bits");
      U.println("Criptograma Beto: " + U.b2x(criptograma) + ", " + criptograma.length);
      U.println("Texto claro Beto: " + new String(textoclaroBeto));
    } else {
      U.println("Tag não verificada!");
    }

  }

    //de Antônio Gonçalves Dias - versão sem acentos ...
  // deste modo, cada caracter é um byte. e aconta do getBytes acerta...
  static String cancaoDoExilio = "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          + "As aves que aqui gorjeiam"
          + "Nao gorjeiam como lah."
          //+ ""
          + "Nosso ceu tem mais estrelas,"
          + "Nossas varzeas tem mais flores."
          + "Nossos bosques tem mais vida,"
          + "Nossa vida mais amores."
          //+ ""
          + "Em cismar, sozinho, aa noite,"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Minha terra tem primores"
          + "Que tais nao encontro eu cah;"
          + "Em cismar — sozinho, aa noite —"
          + "Mais prazer encontro eu lah."
          + "Minha terra tem palmeiras"
          + "Onde canta o sabiah."
          //+ ""
          + "Nao permita Deus que eu morra"
          + "Sem que eu volte para lah;"
          + "Sem que desfrute os primores"
          + "Que nao encontro por cah;"
          + "Sem que ainda aviste as palmeiras"
          + "Onde canta o sabiah.";
}
