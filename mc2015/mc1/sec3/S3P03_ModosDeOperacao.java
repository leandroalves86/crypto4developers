package mc2015.mc1.sec3;

import java.security.Security;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//1.3.3. Modos de operação e suas propriedades
class S3P03_ModosDeOperacao {

  public static void main(String[] a) {
    try {

      Security.addProvider(new BouncyCastleProvider()); // provedor BC

      boolean invertebit = false, trocaBloco = false, perdeBloco = true;
      int i = 1;// de 0 a 15 do 1o. bloco
      byte[] k = U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
      byte[] iv = U.x2b("ABCDEF1234567890ABCDEF1234567890");
      //byte[] textoAna = ("Modo de operacaoModo de operacao").getBytes();
      byte[] textoAna = ("Modo de operacaoModo de operacao"
              + "Modo de operacao").getBytes();

      String aes = "AES/ECB/NoPadding";
      SecretKeySpec ks = new SecretKeySpec(k, "AES");
      Cipher c = Cipher.getInstance(aes, "BC");
      c.init(Cipher.ENCRYPT_MODE, ks);
      byte[] criptogramaAna = c.doFinal(textoAna);
      byte[] criptograma = criptogramaAna.clone();
      // modificando um byte.
      if (invertebit) {
        criptograma[i] = (byte) (criptograma[i] ^ (byte) 0x01);
      }
      if (trocaBloco) {
        for (int j = 0; j < 16; j++) {
          byte temp = criptograma[j];
          criptograma[j] = criptograma[j + 16];
          criptograma[j + 16] = temp;
        }
      }
      if (perdeBloco) {
        byte[] temp = new byte[criptograma.length - 16];
        for (int j = 0; j < temp.length; j++) {
          temp[j] = criptograma[j + 16];
        }
        criptograma = temp;
      }

      System.out.println("Chave: " + U.b2x(k));
      System.out.println("iv: " + U.b2x(iv));
      System.out.println("Texto claro  Ana: " + new String(textoAna));
      //System.out.println("Criptograma  Ana: " + U.b2x(criptogramaAna));
      
      c.init(Cipher.DECRYPT_MODE, ks);
      byte[] TextoBeto = c.doFinal(criptograma);
      System.out.println("\nTeste 1: " + aes);
      System.out.println("Criptograma: " + U.b2x(criptograma));
      System.out.println("Texto claro: " + new String(TextoBeto));

      aes = "AES/CBC/NoPadding";
      ks = new SecretKeySpec(k, "AES");
      c = Cipher.getInstance(aes, "BC");
      AlgorithmParameterSpec aps = new IvParameterSpec(iv);
      c.init(Cipher.ENCRYPT_MODE, ks, aps);
      criptogramaAna = c.doFinal(textoAna);
      criptograma = criptogramaAna.clone();
      // modificando 1 bit de um byte.
      if (invertebit) {
        criptograma[i] = (byte) (criptograma[i] ^ (byte) 0x01);
      }
      if (trocaBloco) {
        for (int j = 0; j < 16; j++) {
          byte temp = criptograma[j];
          criptograma[j] = criptograma[j + 16];
          criptograma[j + 16] = temp;
        }
      }
      if (perdeBloco) {
        byte[] temp = new byte[criptograma.length - 16];
        for (int j = 0; j < temp.length; j++) {
          temp[j] = criptograma[j + 16];
        }
        criptograma = temp;
      }
      c.init(Cipher.DECRYPT_MODE, ks, aps);
      TextoBeto = c.doFinal(criptograma);
      System.out.println("\nTeste 2: " + aes);
      System.out.println("Criptograma: " + U.b2x(criptograma));
      System.out.println("Texto claro: " + new String(TextoBeto));

      aes = "AES/CFB/NoPadding";
      ks = new SecretKeySpec(k, "AES");
      c = Cipher.getInstance(aes, "BC");
      aps = new IvParameterSpec(iv);
      c.init(Cipher.ENCRYPT_MODE, ks, aps);
      criptogramaAna = c.doFinal(textoAna);
      criptograma = criptogramaAna.clone();
      // modificando 1 bit de um byte.
      if (invertebit) {
        criptograma[i] = (byte) (criptograma[i] ^ (byte) 0x01);
      }
      if (trocaBloco) {
        for (int j = 0; j < 16; j++) {
          byte temp = criptograma[j];
          criptograma[j] = criptograma[j + 16];
          criptograma[j + 16] = temp;
        }
      }
      if (perdeBloco) {
        byte[] temp = new byte[criptograma.length - 16];
        for (int j = 0; j < temp.length; j++) {
          temp[j] = criptograma[j + 16];
        }
        criptograma = temp;
      }
      c.init(Cipher.DECRYPT_MODE, ks, aps);
      TextoBeto = c.doFinal(criptograma);
      System.out.println("\nTeste 3: " + aes);
      System.out.println("Criptograma: " + U.b2x(criptograma));
      System.out.println("Texto claro: " + new String(TextoBeto));

      aes = "AES/OFB/NoPadding";
      ks = new SecretKeySpec(k, "AES");
      c = Cipher.getInstance(aes, "BC");
      aps = new IvParameterSpec(iv);
      c.init(Cipher.ENCRYPT_MODE, ks, aps);
      criptogramaAna = c.doFinal(textoAna);
      criptograma = criptogramaAna.clone();
      // modificando 1 bit de um byte.
      if (invertebit) {
        criptograma[i] = (byte) (criptograma[i] ^ (byte) 0x01);
      }
      if (trocaBloco) {
        for (int j = 0; j < 16; j++) {
          byte temp = criptograma[j];
          criptograma[j] = criptograma[j + 16];
          criptograma[j + 16] = temp;
        }
      }
      if (perdeBloco) {
        byte[] temp = new byte[criptograma.length - 16];
        for (int j = 0; j < temp.length; j++) {
          temp[j] = criptograma[j + 16];
        }
        criptograma = temp;
      }
      c.init(Cipher.DECRYPT_MODE, ks, aps);
      TextoBeto = c.doFinal(criptograma);
      System.out.println("\nTeste 4: " + aes);
      System.out.println("Criptograma: " + U.b2x(criptograma));
      System.out.println("Texto claro: " + new String(TextoBeto));

      aes = "AES/CTR/NoPadding";
      ks = new SecretKeySpec(k, "AES");
      c = Cipher.getInstance(aes, "BC");
      aps = new IvParameterSpec(iv);
      c.init(Cipher.ENCRYPT_MODE, ks, aps);
      criptogramaAna = c.doFinal(textoAna);
      criptograma = criptogramaAna.clone();
      // modificando 1 bit de um byte.
      if (invertebit) {
        criptograma[i] = (byte) (criptograma[i] ^ (byte) 0x01);
      }
      if (trocaBloco) {
        for (int j = 0; j < 16; j++) {
          byte temp = criptograma[j];
          criptograma[j] = criptograma[j + 16];
          criptograma[j + 16] = temp;
        }
      }
      if (perdeBloco) {
        byte[] temp = new byte[criptograma.length - 16];
        for (int j = 0; j < temp.length; j++) {
          temp[j] = criptograma[j + 16];
        }
        criptograma = temp;
      }
      c.init(Cipher.DECRYPT_MODE, ks, aps);
      TextoBeto = c.doFinal(criptograma);
      System.out.println("\nTeste 5: " + aes);
      System.out.println("Criptograma: " + U.b2x(criptograma));
      System.out.println("Texto claro: " + new String(TextoBeto));

    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
  }
}
