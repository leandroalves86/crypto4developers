/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import javax.crypto.spec.PSource;
import org.bouncycastle.jce.provider.*;

// 1.3.8. Transporte de chave simétrica com encriptação assimétrica
public class S3P10_TransporteChaveSecretaRSAOAEP {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // Este é o par de chaves do beto
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
    kpg.initialize(3072);
    KeyPair kp = kpg.generateKeyPair(); // Beto cria um par de chaves
    Key pubk = kp.getPublic(), privk = kp.getPrivate();

    // configurações da criptografia assimétrica para Ana e Beto
    OAEPParameterSpec OAEPps = new OAEPParameterSpec("SHA-512", "MGF1",
            MGF1ParameterSpec.SHA512, PSource.PSpecified.DEFAULT);
    Cipher x = Cipher.getInstance("RSA/None/OAEPwithSHA512andMGF1Padding","BC");

    // configurações da criptografia simétrica para Ana a e Beto
    Cipher c = Cipher.getInstance("AES/CTR/PKCS7Padding", "BC");
    IvParameterSpec ivps
            = new IvParameterSpec(U.x2b("ABCDEF1234567890ABCDEF1234567890"));

    // Chave secreta compartilhada entre Ana e Beto
    KeyGenerator gAna = KeyGenerator.getInstance("AES", "BC");
    gAna.init(256); //chave AES de 128 bits (compartilhada)
    Key skAna = gAna.generateKey(); // Ana cria uma chave pseudoaleatória

    // Ana encripta alguma mensagem ...
    c.init(Cipher.ENCRYPT_MODE, skAna, ivps);
    byte[] textoclaroAna = "Teste do transporte de chaves".getBytes();
    byte[] criptograma = c.doFinal(textoclaroAna);

    // Encriptação pela Ana da chave secreta com a Chave pública do Beto.
    x.init(Cipher.ENCRYPT_MODE, pubk, OAEPps);
    byte[] chaveEncriptada = x.doFinal(skAna.getEncoded());

        // Aqui começa a parte do Beto
    // decriptação da chave secreta pelo Beto com a sua chave privada
    x.init(Cipher.DECRYPT_MODE, privk, OAEPps);
    byte[] chaveBytes = x.doFinal(chaveEncriptada);
    //recuperacao da chave secreta transportada
    SecretKeySpec skBeto = new SecretKeySpec(chaveBytes, "AES");

    //decriptando alguma coisa com a chave secreta
    c.init(Cipher.DECRYPT_MODE, skBeto, ivps);
    byte[] textoClaroBeto = c.doFinal(criptograma);

    U.println("Texto claro  da Ana: " + U.b2s(textoclaroAna));
    U.println("Texto claro do Beto: " + new String(textoClaroBeto));
  }
}
