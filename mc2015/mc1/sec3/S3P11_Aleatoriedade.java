/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;
import java.security.SecureRandom;

//1.3.10. Aleatoriedade e geração de números pseudoaleatórios
public class S3P11_Aleatoriedade {

  public static void main(String[] args) {
    try {
      System.out.println("Teste 1: Dispersão estatística - SecureRandom - SUN");
      SecureRandom sr1 = SecureRandom.getInstance("SHA1PRNG", "SUN");
      SecureRandom sr2 = SecureRandom.getInstance("SHA1PRNG", "SUN");
      SecureRandom sr3 = SecureRandom.getInstance("SHA1PRNG", "SUN");
      System.out.println("i , sr1 , sr2, sr3");
      for (int i = 0; i < 100; i++) {
        U.println(i + ","+
            sr1.nextInt(10000)+","+sr2.nextInt(10000)+","+sr3.nextInt(10000));
      }
      System.out.println("Teste 2: Imprevisibilidade - SecureRandom - SUN");
      SecureRandom sr4 = SecureRandom.getInstance("SHA1PRNG", "SUN");
      SecureRandom sr5 = SecureRandom.getInstance("SHA1PRNG", "SUN");
      byte[] seed = sr4.generateSeed(32);
      sr4.setSeed(seed); sr5.setSeed(seed); // mesma semente
      System.out.println("i , sr4 , sr5");
      for (int i = 0; i < 100; i++) {
        U.println(i + "," + sr4.nextInt(10000) + ","+ sr5.nextInt(10000));
      }

      System.out.println("Teste 3: Dispersão estatística - SecureRandom - MSCAPI");
      SecureRandom sr6 = SecureRandom.getInstance("Windows-PRNG", "SunMSCAPI");
      SecureRandom sr7 = SecureRandom.getInstance("Windows-PRNG", "SunMSCAPI");
      SecureRandom sr8 = SecureRandom.getInstance("Windows-PRNG", "SunMSCAPI");
      for (int i = 0; i < 100; i++) {
        if (i == 0) {
          System.out.println("i , sr6 , sr7, sr8");
        }
        System.out.println(i + ", "
                + sr6.nextInt(10000) + ", "
                + sr7.nextInt(10000) + ", "
                + sr8.nextInt(10000));
      }
      System.out.println("Teste 4: Imprevisibilidade - SecureRandom - MSCAPI");
      SecureRandom sr9 = SecureRandom.getInstance("Windows-PRNG", "SunMSCAPI");
      SecureRandom sr0 = SecureRandom.getInstance("Windows-PRNG", "SunMSCAPI");
      seed = sr9.generateSeed(32);
      sr9.setSeed(seed); // mesma semente
      sr0.setSeed(seed);
      for (int i = 0; i < 100; i++) {
        if (i == 0) {
          System.out.println("i , sr9 , sr0");
        }
        System.out.println(i + "," + sr9.nextInt(10000) + ","
                + sr0.nextInt(10000));
      }

      System.out.println("Teste 5: Dispersão estatística - Random");
      Random r1 = new Random(), r2 = new Random(), r3 = new Random();
      for (int i = 0; i < 100; i++) {
        if (i == 0) {
          System.out.println("i , r1 , r2, r3");
        }
        System.out.println(i + "," + r1.nextInt(10000) + ","
                + r2.nextInt(10000) + ","
                + r3.nextInt(10000));
      }

      System.out.println("Teste 6: Imprevisibilidade - Random");
      Random r4 = new Random(), r5 = new Random();
      long lseed = sr9.nextLong();
      r4.setSeed(lseed);
      r5.setSeed(lseed);
      for (int i = 0; i < 100; i++) {
        if (i == 0) {
          System.out.println("i , r4 , r5");
        }
        System.out.println(i + "," + r4.nextInt(10000) + ","
                + r5.nextInt(10000));
      }

    } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
      System.out.println(e);
    }
  }
}
