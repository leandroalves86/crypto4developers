/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;
public class S3P12_IncrementoDeIV {
  
  public static void main (String args[]){
    byte[] ivAlice = U.x2b("01"+"000000000000000000000000000000");
    byte[] ivBob   = U.x2b("02"+"000000000000000000000000000000");
    U.println("Configuração inicial:");
    U.println("IV Ana:"+U.b2x(ivAlice)+", IV Beto:"+U.b2x(ivBob)); 
    U.println("Uso:");
    for (int i = 0; i < 3; i++){
      stdIncIV(ivAlice,1);
      stdIncIV(ivBob,1);
      U.println("IV Ana:"+U.b2x(ivAlice)+", IV Beto:"+U.b2x(ivBob)); 
      delay(1);
    }
    U.println("...                                           ...");
    ivAlice = U.x2b("00"+"CDEF1234567890ABCDEF1234567890");
    ivBob   = U.x2b("01"+"ABCDEF1234567890ABCDEF12345678");
    for (int i = 0; i < 3; i++){
      stdIncIV(ivAlice,1);
      stdIncIV(ivBob,1);
      U.println("IV Ana:"+U.b2x(ivAlice)+", IV Beto:"+U.b2x(ivBob)); 
      delay(1);
    }
    U.println("...                                           ...");
    U.println("HORA DE TROCAR A CHAVE!!!!!");
    ivAlice = U.x2b("00"+"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFB");
    ivBob   = U.x2b("01"+"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC");
    for (int i = 0; i < 5; i++){
      stdIncIV(ivAlice,1);
      stdIncIV(ivBob,1);
      U.println("IV Ana:"+U.b2x(ivAlice)+", IV Beto:"+U.b2x(ivBob)); 
      delay(1);
    }
  }
  
  // incremento padrão para IV no modo CTR
  public static void stdIncIV(byte[] a, int mark) {
    int l = 0;
    if (mark >= 0 && mark < a.length) { l = mark;}
    for (int i = a.length - 1; i >= l; i--) {
      if ((a[i] & 0xFF) != 0xFF) {
        a[i] = (byte) (a[i] + 1);
        break;
      } else {
        a[i] = (byte) (0x00);
        continue;
      }
    }
  }
  
  public static void delay(int seg) {
    long x = seg * 1000;
    long t = System.currentTimeMillis();
    while ((System.currentTimeMillis() - t) < x) {
      ; // atraso de x segundos
    }
  }
}

