/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

public class PerformanceTest {

  public static int loop = 10000; //10k
  //public static int loop = 100000; // 100k
  //public static int loop = 1000000; // 1000k

  public static final byte[] pt1 = "Alexandre Braga1".getBytes();
  public static final byte[] pt2 = "Alexandre Braga1".getBytes();
  public static final byte[] pt3 = "Alexandre Braga1".getBytes();

  public static final byte[][] key = new byte[3][];

  public static int[] wk = {AES1.KEYSIZE_128, AES1.KEYSIZE_192, AES1.KEYSIZE_256};

  static {
    key[0] = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF");
    key[1] = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF");
    key[2] = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF");
  }

  public static byte[] dec1 = new byte[pt1.length];
  public static byte[] dec2 = new byte[pt2.length];
  public static byte[] dec3 = new byte[pt3.length];

  public static byte[] ct1 = new byte[pt1.length];
  public static byte[] ct2 = new byte[pt2.length];
  public static byte[] ct3 = new byte[pt3.length];

  public static long[] enc = new long[3];
  public static long[] dec = new long[3];

  public static void main(String[] args) {
    aesEncDecTest();
    //aesLoadTest();
  }

  public static void aesLoadTest() {
    long fTime, iTime;
    //int loop = 1000000;
    AES1 aes1 = new AES1();
    AES2 aes2 = new AES2();
    AES3 aes3 = new AES3();

    iTime = System.currentTimeMillis();
    for (int i = 0; i < loop; i++) {aes1 = new AES1();}
    fTime = System.currentTimeMillis();
    enc[0] = fTime - iTime;

    iTime = System.currentTimeMillis();
    for (int i = 0; i < loop; i++) {aes2 = new AES2();}
    fTime = System.currentTimeMillis();
    enc[1] = fTime - iTime;

    iTime = System.currentTimeMillis();
    for (int i = 0; i < loop; i++) {aes3 = new AES3();}
    fTime = System.currentTimeMillis();
    enc[2] = fTime - iTime;

    //--
    for (int i = 0; i < enc.length; i++){System.out.println("LOAD " + enc[i]);}

    for (int ks = 0; ks < wk.length; ks++) {

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes1.init(key[ks], wk[ks]);}
      fTime = System.currentTimeMillis();
      enc[0] = fTime - iTime;

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes2.init(key[ks], wk[ks]);}
      fTime = System.currentTimeMillis();
      enc[1] = fTime - iTime;

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes3.init(key[ks], wk[ks]);}
      fTime = System.currentTimeMillis();
      enc[2] = fTime - iTime;

      System.out.println("Tamanho da chave = " + key[ks].length * 8 + " bits");
      for (int i = 0; i < enc.length; i++){System.out.println("INIT " + enc[i]);}
    }
  }

  public static void aesEncDecTest() {
    long fTime, iTime;

    AES1 aes1 = new AES1();
    AES2 aes2 = new AES2();
    AES3 aes3 = new AES3();

    System.out.println("Teste da encriptação");
    for (int ks = 0; ks < wk.length; ks++) {
      aes1.init(key[ks], wk[ks]);
      aes2.init(key[ks], wk[ks]);
      aes3.init(key[ks], wk[ks]);

      System.out.println("Tamanho da chave = " + key[ks].length * 8 + " bits");

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes1.encrypt(pt1, ct1);}
      fTime = System.currentTimeMillis();
      enc[0] = fTime - iTime;

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes1.decrypt(ct1, dec1);}
      fTime = System.currentTimeMillis();
      dec[0] = fTime - iTime;

      //--
      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes2.encrypt(pt2, ct2);}
      fTime = System.currentTimeMillis();
      enc[1] = fTime - iTime;

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes2.decrypt(ct2, dec2);}
      fTime = System.currentTimeMillis();
      dec[1] = fTime - iTime;

      //--
      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes3.encrypt(pt3, ct3);}
      fTime = System.currentTimeMillis();
      enc[2] = fTime - iTime;

      iTime = System.currentTimeMillis();
      for (int i = 0; i < loop; i++) {aes3.decrypt(ct3, dec3);}
      fTime = System.currentTimeMillis();
      dec[2] = fTime - iTime;

      //--
      for (int i = 0; i < enc.length; i++){System.out.println("ENC " + enc[i]);}

      for (int i = 0; i < dec.length; i++){System.out.println("DEC " + dec[i]);}
    }
  }

}
