/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

public class UtilAES {

    private static final int Nb = 4;
    private static String[] dig = {"0", "1", "2", "3", "4", "5", "6", "7",
        "8", "9", "a", "b", "c", "d", "e", "f"};
// hex: print a byte as two hex digits

    public static String hex(byte a) {
        return dig[(a & 0xff) >> 4] + dig[a & 0x0f];
    }

    public static void printArray(String name, byte[] a) {
        System.out.print(name + " ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(hex(a[i]) + " ");
        }
        System.out.println();
    }

    public static void printArray(String name, byte[][] s) {
        System.out.print(name + " ");
        for (int c = 0; c < Nb; c++) {
            for (int r = 0; r < 4; r++) {
                System.out.print(hex(s[r][c]) + " ");
            }
        }
        System.out.println();
    }
    
    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16) {
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                } else {
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
                }
            }
            return str.toUpperCase();
        }
    }
    
   
// toState: toState in to state
    public static void toState(byte[][] state, byte[] in) {
        int inLoc = 0;
        for (int c = 0; c < Nb; c++) {
            for (int r = 0; r < 4; r++) {
                state[r][c] = in[inLoc++];
            }
        }
    }
// toState: toState state to out
    public static void fromState(byte[] out, byte[][] state) {
        int outLoc = 0;
        for (int c = 0; c < Nb; c++) {
            for (int r = 0; r < 4; r++) {
                out[outLoc++] = state[r][c];
            }
        }
    }
    
    public static void generateArrayCode(String name, byte[] a) {
        String str = ", ";
        System.out.print("private byte[] "+name + " = {");
        for (int i = 0; i < a.length; i++) {
            if (i == a.length-1) str = "}";
            System.out.print("(byte)0x"+hex(a[i]) + str);
        }
        System.out.println(";");
    }
    
}
