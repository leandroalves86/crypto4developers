/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

public class TestBlockCipher {
    
    public static final byte[] key128 = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF");
    public static final byte[] key192 = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF");
    public static final byte[] key256 = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF");
    
    public static final byte[] iv = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF");
    
    public static final byte[] pt1 = "Alexandre Braga1Alexandre Braga123456".getBytes();
    public static byte[] dec1 = new byte[pt1.length];
    public static byte[] ct1 = new byte[pt1.length];

    public static void main(String[] args) {
        testECB(args);
        testCBC(args);
        testCTR(args);
        //performanceModesPadding(args);
        //performanceModesNoPadding(args);
    }
    
    public static void testECB(String[] args) {
        BlockCipher aes = new BlockCipher(BlockCipher.ECB,BlockCipher.PADDING);
        aes.init(key128,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key128,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key192,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key192,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key256,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key256,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
    }
    
    public static void testCBC(String[] args) {
        BlockCipher aes = new BlockCipher(BlockCipher.CBC,BlockCipher.PADDING);
        aes.setIV(iv);
        aes.init(key128,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key128,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key192,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key192,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key256,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key256,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
    }
    
    public static void testCTR(String[] args) {
        BlockCipher aes = new BlockCipher(BlockCipher.CTR,BlockCipher.PADDING);
        byte[] iv = UtilAES.hexToBytes("0123456789ABCDEF0000000000000000");
        aes.setIV(iv);
        aes.init(key128,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key128,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key192,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key192,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
        
        aes.init(key256,BlockCipher.ENCRYPT);
        ct1 = aes.doCipher(pt1);
        aes.init(key256,BlockCipher.DECRYPT);
        dec1 = aes.doCipher(ct1);
        System.out.println(new String(dec1));
    }
    
    public static void performanceModesPadding(String[] args) {
        int loop = 10000000;
        long iTime,fTime;
        BlockCipher aes = new BlockCipher(BlockCipher.ECB,BlockCipher.PADDING);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("ECB128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("ECB128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
        
        aes = new BlockCipher(BlockCipher.CBC,BlockCipher.PADDING);
        aes.setIV(iv);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CBC128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CBC128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
        
        aes = new BlockCipher(BlockCipher.CTR,BlockCipher.PADDING);
        byte[] ivCTR = UtilAES.hexToBytes("0123456789ABCDEF0000000000000000");
        aes.setIV(ivCTR);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CTR128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CTR128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
    }
    
    public static void performanceModesNoPadding(String[] args) {
        int loop = 10000000;
        long iTime,fTime;
        BlockCipher aes = new BlockCipher(BlockCipher.ECB,BlockCipher.NOPADDING);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("ECB128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("ECB128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
        
        aes = new BlockCipher(BlockCipher.CBC,BlockCipher.NOPADDING);
        aes.setIV(iv);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CBC128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CBC128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
        
        aes = new BlockCipher(BlockCipher.CTR,BlockCipher.NOPADDING);
        byte[] ivCTR = UtilAES.hexToBytes("0123456789ABCDEF0000000000000000");
        aes.setIV(ivCTR);
        aes.init(key128,BlockCipher.ENCRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ct1 = aes.doCipher(pt1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CTR128ENC "+(fTime-iTime));
        aes.init(key128,BlockCipher.DECRYPT);
        iTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            dec1 = aes.doCipher(ct1);
        }
        fTime = System.currentTimeMillis();
        System.out.println("CTR128DEC "+(fTime-iTime));
        
        System.out.println(new String(dec1));
    }
}
