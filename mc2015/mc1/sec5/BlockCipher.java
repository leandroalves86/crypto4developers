/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

public class BlockCipher {

    private AES3 aes;
    private byte[] key;
    private byte[] iv;
    private boolean enc;
    private boolean padding;
    private int opMode;
    public static final int blockSize = 128/8;
    public static final int CBC = 10;
    public static final int ECB = 20;
    public static final int CTR = 30;
    public static final boolean ENCRYPT = true;
    public static final boolean DECRYPT = false;
    public static final boolean PADDING   = true;
    public static final boolean NOPADDING = false;
    
    byte[] inBlock = new byte[blockSize];
    byte[] outBlock = new byte[blockSize];

    public BlockCipher(int opMode, boolean padding) {
        this.padding = padding;
        this.opMode = opMode;
        aes = new AES3();
    }

    public void init(byte[] key, boolean enc) {
        this.key = key;
        this.enc = enc;
        int NkIn = key.length/4;
        aes.init(key, NkIn);
    }

    public void setIV (byte[] iv){
        if (this.opMode != ECB) {
            this.iv = iv;
        }
    }
    
    private void doECB(byte[] in, byte[] out) {
        int numBlocks = (int)(in.length/blockSize);

        for (int i = 0; i < numBlocks; i++) {
            int offset = i * blockSize;
            for (int j = 0; j < blockSize; j++) {
                inBlock[j] = in[offset + j];
            }
            if (enc == ENCRYPT) {
                aes.encrypt(inBlock, outBlock);
            } else if (enc == DECRYPT) {
                aes.decrypt(inBlock, outBlock);
            }

            for (int j = 0; j < blockSize; j++) {
                out[offset + j] = outBlock[j];
            }
        }
    }
    
    private void doCBC(byte[] in, byte[] out) {
        int numBlocks = (int)(in.length/blockSize);
        byte[] inX = new byte[blockSize];
        byte[] outX = new byte[blockSize];
        for (int i = 0; i < numBlocks; i++) {
            int offset = i * blockSize;
            for (int j = 0; j < blockSize; j++) {
                inBlock[j] = in[offset + j];
            }
            
            if (i == 0) {copy(iv,outX);}

            if (enc == ENCRYPT) {
                xor(inBlock,outX,inX);
                aes.encrypt(inX,outBlock);
                copy(outBlock,outX);
            }
            
            if (enc == DECRYPT) {
                aes.decrypt(inBlock, inX);
                xor(inX,outX,outBlock);
                copy(inBlock,outX);
            }

            for (int j = 0; j < blockSize; j++) {
                out[offset + j] = outBlock[j];
            }
        }    
    }
    
    private void doCTR(byte[] in, byte[] out) {
        int numBlocks = (int)(in.length/blockSize);
        int sizeCTR   = blockSize/2;// metade do IV é o contador interno
        byte[] inCTR = new byte[blockSize];
        byte[] outCTR = new byte[blockSize];
        for (int i = 0; i < numBlocks; i++) {
            int offset = i * blockSize;
            for (int j = 0; j < blockSize; j++) {
                inBlock[j] = in[offset + j];
            }
            
            if (i == 0) {copy(iv,inCTR);}
            else        {standardIncrement(inCTR,sizeCTR);}
            
            if (enc == ENCRYPT || enc == DECRYPT) {
                aes.encrypt(inCTR, outCTR);
                xor(outCTR,inBlock,outBlock);
            }

            for (int j = 0; j < blockSize; j++) {
                out[offset + j] = outBlock[j];
            }
        }
    }
    
    private byte[] doPadding(byte[] in){
        byte pad[] = {16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
        byte padding = (byte) (in.length%blockSize);
        int numBlocks = (int)(in.length/blockSize);
        byte[] padded = new byte[(numBlocks+1)*blockSize];
        
        for (int i = 0; i < in.length; i++){
            padded[i] = in[i];
        }
        for (int i = in.length; i < padded.length; i++){
            padded[i] = (byte) pad[padding];
        }
        return padded;
    }
    
    private byte[] undoPadding(byte[] padded){
        byte[] out = new byte[padded.length - padded[padded.length-1]];
        for (int i = 0; i < out.length; i++){
            out[i] = padded[i];
        }
        return out;
    }
    
    
    public byte[] doCipher(byte[] in) /* throws Exception*/  {
        byte[] inPadded = null, outPadded = null, out = null;
        if (padding){
            if(enc == ENCRYPT) { inPadded = doPadding(in);}
            if(enc == DECRYPT) { inPadded = in;}
        } else {
            inPadded = in;
        }
        outPadded = new byte[inPadded.length]; 
             if (this.opMode == ECB) { doECB(inPadded, outPadded);}
        else if (this.opMode == CBC) { doCBC(inPadded, outPadded);}
        else if (this.opMode == CTR) { doCTR(inPadded, outPadded);}
        if (padding){
            if (enc == ENCRYPT) { out = outPadded; }
            if (enc == DECRYPT) { out = undoPadding(outPadded); }
        } else {
            out = outPadded;
        }
        return out;
    }

    private void xor(byte[] a, byte[] b, byte[] x) {
        for (int i = 0; i < x.length; i++) {
            x[i] = (byte) ((a[i] ^ b[i]) & 0xFF);
        }
    }
    
    private void copy(byte[] a, byte[] b) {
        for (int i = 0; i < b.length; i++) {
            b[i] = a[i];
        }
    }
    
    private void standardIncrement(byte[] a, int mark){
        int l = 0;
        if (mark >= 0 && mark < a.length) l = mark;
            for (int i = a.length-1; i >=l; i--) {
                if ((a[i]&0xFF) != 0xFF) { 
                    a[i] = (byte)(a[i]+1); break;
                }
                else {
                    a[i] = (byte)(  0x00); 
                    continue;
                }
            }
    }
    
    /*
    public static void main (String args[]){
        byte[] key = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF");
        byte[] pt1 = "Alexandre Braga11".getBytes();
        byte[] pt2 = "Alexandre Braga1".getBytes();
        byte[] ct1 = new byte[blockSize];
        byte[] ct2 = new byte[blockSize];
        byte[] pt3 = new byte[blockSize];
        byte[] pt4 = new byte[blockSize];
        byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF1234567890ABCDEF");
        byte[] inX = new byte[blockSize];
        byte[] ctr = new byte[blockSize];
        AESOpModes aes = new AESOpModes(CBC,NOPADDING);
        aes.init(key,ENCRYPT);
        aes.setIV(iv);
        // cbc enc
        aes.copy(iv,ctr);
        
        System.out.println(new String(pt1));
        System.out.println(new String(pt2));
        
        aes.xor(pt1,ctr,inX);
        aes.aes.encrypt(inX, ct1);
        aes.copy(ct1,ctr);
        
        aes.xor(pt2,ctr,inX);
        aes.aes.encrypt(inX, ct2);
        //aes.copy(ct2,ctr);

        // cbc dec
        aes.copy(iv,ctr);
        
        aes.aes.decrypt(ct1, inX);
        aes.xor(inX,ctr,pt3);
        
        aes.copy(ct1,ctr);
        
        aes.aes.decrypt(ct2, inX);
        aes.xor(inX,ctr,pt4);
        //aes.copy(pt4,ctr);
        
        System.out.println(new String(pt3));
        System.out.println(new String(pt4));
    }
    */
    
    /*
    public static void main(String[] args){
        //byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF0000000000000000");
        byte[] iv1 = UtilAES.hexToBytes("12340000");
        byte[] iv2 = UtilAES.hexToBytes("123400000000");
        AESOpModes aes = new AESOpModes(CTR,PADDING);
        for (int i = 0; i < 256*256 ; i++ ) { 
            aes.standardIncrement(iv1, iv1.length/2);
            System.out.println(i+1+" "+UtilAES.bytesToHex(iv1));
        }
        //for (int i = 0; i < 512 ; i++ ) { 
        //    aes.wrongStandardIncrement(iv2, 2);
        //    System.out.println(i+1+" "+UtilAES.bytesToHex(iv2));
        //}
    }
    */
    
    
    public static void main(String[] args){
        byte[] key = UtilAES.hexToBytes("0123456789ABCDEF0123456789ABCDEF");
        byte[] pt1 = "Alexandre Braga1".getBytes();
        byte[] ct1 = new byte[0], pt2 = new byte[0];
        //byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF0000000000000000");
        byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF1234567890ABCDEF");
        BlockCipher aes = new BlockCipher(CBC,PADDING);
        aes.setIV(iv);
        aes.init(key,ENCRYPT);
        try{ ct1 = aes.doCipher(pt1); } catch(Exception e) { }
        byte hex = 0x00;
        String ex;
        for (int i = 0; i < 256; i++){
            ct1[ct1.length-1] = (byte) (i&0xFF);
            aes.init(key,DECRYPT);
            long itime,ftime;
            ex = "";
            itime = System.nanoTime();
            try{
                pt2 = aes.doCipher(ct1);
            } catch(Exception e) { ex = e.toString();}
            ftime = System.nanoTime();
            System.out.print(i+" ");
            System.out.print(ftime-itime);
            //System.out.print(" "+new String(pt2));
            System.out.print(" "+ex);
            System.out.println();
        }
    }  
    
  
    /*
    public static void main(String[] args){
        //byte[] key = UtilAES.hexToBytes("ffffffffffffffffffffffffffffffff");
        byte[] key = UtilAES.hexToBytes("00000000000000000000000000000000");
        String pt1str = 
"                            _____                              \n"+
"                        .d88888888bo.                          \n"+
"                      .d8888888888888b.                        \n"+
"                      8888888888888888b                        \n"+
"                      888888888888888888                       \n"+
"                      888888888888888888                       \n"+
"                       Y8888888888888888                       \n"+
"                 ,od888888888888888888P                        \n"+
"              .'`Y8P'```'Y8888888888P'                         \n"+
"            .'_   `  _     'Y88888888b                         \n"+
"           /  _`    _ `      Y88888888b   ____                 \n"+
"        _  | /  \\  /  \\      8888888888.d888888b.            \n"+
"       d8b | | /|  | /|      8888888888d8888888888b            \n"+
"      8888_\\ \\_|/  \\_|/      d888888888888888888888b        \n"+
"      .Y8P  `'-.            d88888888888888888888888           \n"+
"     /          `          `      `Y8888888888888888           \n"+
"     |                        __    888888888888888P           \n"+
"      \\                       / `   dPY8888888888P'           \n"+
"       '._                  .'     .'  `Y888888P`              \n"+
"          `'-.,__    ___.-'    .-                              \n"+
"         jgs  `-._````  __..--'`                               \n"+
"                  ``````                                       \n";
byte[] pt1 = pt1str.getBytes();
        byte[] ct1, pt2;
        String ctASCII;
        byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF0000000000000000");
        //byte[] iv = UtilAES.hexToBytes("1234567890ABCDEF1234567890ABCDEF");
        AESOpModes aes = new AESOpModes(CTR,PADDING);
        aes.setIV(iv);
        aes.init(key,ENCRYPT);
        ct1 = aes.doCipher(pt1);
        ctASCII= UtilAES.bytesToHex(ct1);
        for (int i =0; i < ctASCII.length(); i++){
            System.out.print(ctASCII.charAt(i)+";");
            if ((i>0)&&(i % 128 == 0))System.out.println();
        }
        //ctASCII= UtilAES.bytesToHex(ct1);
        //for (int i =0; i < ct1.length; i++){
        //    System.out.print(ct1[i]);
        //    if ((i>0)&&(i % 64 == 0))System.out.println();
        //}
        System.out.println();
        aes.init(key,DECRYPT);
        pt2 = aes.doCipher(ct1);
        System.out.println(new String(pt2));
    }
    */
}

class PaddingException extends Exception {

    public PaddingException(){
    
    }
}