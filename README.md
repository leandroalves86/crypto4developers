Crypto4Developers is a repository for source code used in three tutorials related to teaching cryptography to ordinary developers.
Crypto4Developers é um repositório para o código fonte usado em três tutoriais relacionados ao ensino de criptografia para programadores.

A. Braga and R. Dahab, “Introdução à Criptografia para Programadores: Evitando Maus Usos da Criptografia em Sistemas de Software,” 
Capítulo 1 do Caderno de minicursos do XV Simpósio Brasileiro em Segurança da Informação e de Sistemas Computacionais (SBSeg), 2015, pp. 1–50.

A. Braga and R. Dahab, “Criptografia Assimétrica para Programadores: Evitando outros maus usos da criptografia em sistemas de software,” 
Capítulo 2 do Caderno de minicursos do XVIII Simpósio Brasileiro em Segurança da Informação e de Sistemas Computacionais (SBSeg), 2018, pp. 51–100.

A. Braga and R. Dahab, “Introdução à criptografia para administradores de sistemas com TLS, OpenSSL e Apache mod_ssl,” 
Capítulo 5 do Caderno de minicursos do XXXVII Simpósio Brasileiro de Redes de Computadores e Sistemas Distribuídos (SBRC), 2019, pp. 201–250.