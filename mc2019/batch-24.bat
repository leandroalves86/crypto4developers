echo OFF
cls
echo #############################################
echo Listagem 5.1: Descobrindo a versao do OpenSSL
echo #############################################
echo.
echo ON

IF "%1" == "" goto END 

sslscan -verbose %1:443
@echo.
@pause

:END
@echo DONE