echo OFF
cls
echo #############################################
echo Listagem 5.1: Descobrindo a versao do OpenSSL
echo #############################################
echo.
echo ON

openssl dgst -sha256 -r gabarito.txt
@echo.
@pause

openssl dgst -sha256 gabarito.txt 
@echo.
@pause

openssl dgst -sha512 -r gabarito.txt 
@echo.
@pause

openssl dgst -sha3-512 -r gabarito.txt 
@echo.
@pause

openssl dgst -sha256 -hmac 3f44a88d098cdb8a384922e88a30dbe67f7178fd gabarito.txt 
@echo.
@pause

openssl dgst -sha3-512 -hmac 3f44a88d098cdb8a384922e88a30dbe67f7178fd gabarito.txt
@echo.

:END
@echo DONE
