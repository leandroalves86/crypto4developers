echo OFF
cls
echo ###################################################
echo Listagem 5.17. Exemplo de servidor web sem suporte ao TLS 1.3
echo ###################################################
echo.
echo ON

IF "%1" == "" goto END 

openssl s_client -connect %1 -port 443 -tls1_3
@echo.
 
:END
@echo DONE