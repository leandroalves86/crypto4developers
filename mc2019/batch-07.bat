echo OFF
cls
echo ###########################################
echo Listagem 5.7. Prenchimento de blocos do AES
echo ###########################################
echo.
echo ON

type i1.txt
type iF.txt
type i16F.txt
@echo.
@pause

openssl enc -e -AES-128-ECB -in i16F.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -nopad -out o16F.txt
openssl enc -d -AES-128-ECB -in o16F.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -nopad 
@echo.
@pause

openssl enc -e -AES-128-ECB -in i1.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out o1.txt
openssl enc -d -AES-128-ECB -in o1.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out o1_2.txt -nopad 
type o1_2.txt
@echo.
@pause

openssl enc -e -AES-128-ECB -in iF.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out oF.txt
openssl enc -d -AES-128-ECB -in oF.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out oF_2.txt -nopad 
type oF_2.txt
@echo.
@pause

openssl enc -e -AES-128-ECB -in i16F.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out o16F.txt
openssl enc -d -AES-128-ECB -in o16F.txt -K 6CB40CAEF6FDEDB31DBD908256F6327A -out o16F_2.txt -nopad 
@echo.
type o16F_2.txt

:END
@echo.
@echo DONE