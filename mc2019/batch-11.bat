echo OFF
cls
echo ##########################################################
echo Listagem 5.11. Limite de tamanho para o criptograma no RSA
echo ##########################################################
echo.
echo ON

type gabarito.txt
@echo.
@pause

openssl rsautl -encrypt -in gabarito.txt -pubin -inkey chave_pub.rsa -out o.rsac 
@echo.
@pause

openssl rsautl -encrypt -oaep -in gabarito.txt -pubin -inkey chave_pub.rsa -out o.oaep
@echo.
@pause

type medio.txt
@echo.
@pause

openssl rsautl -encrypt -in medio.txt -pubin -inkey chave_pub.rsa -out o.rsac 
@echo.
@pause

openssl rsautl -encrypt -oaep -in medio.txt -pubin -inkey chave_pub.rsa -out o.oaep
@echo.
@pause

openssl rsautl -encrypt -in longo.txt -pubin -inkey chave_pub.rsa -out o.rsac 
@echo.
@pause

openssl rsautl -encrypt -oaep -in longo.txt -pubin -inkey chave_pub.rsa -out o.oaep
@echo.

:END
@echo DONE

