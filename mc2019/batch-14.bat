echo OFF
cls
echo #########################################################
echo Listagem 5.14. As curvas elípticas disponíveis no OpenSSL
echo #########################################################
echo.
echo ON

openssl ecparam -list_curves | more
@echo.
@pause

openssl ecparam -list_curves | find "secp" 
@echo.
@pause

openssl ecparam -list_curves | find "sect" 
@echo.
@pause

openssl ecparam -list_curves | find "prime" 
@echo.
@pause

openssl ecparam -list_curves | find "brainpool" 
@echo.

:END
@echo DONE