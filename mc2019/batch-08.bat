echo OFF
cls
echo ######################################################################
echo Listagem 5.8. Os modos de opera��o e como toleram erros no criptograma
echo ######################################################################
echo.
echo ON

openssl enc -e -a -aes-128-ecb -in alex.txt -K 6CB40CAEF6FDEDB31DBD908256F63276 -out alex.ecb
@echo.
@pause

openssl enc -e -a -aes-128-cbc -in alex.txt -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A -out alex.cbc
@echo.
@pause

openssl enc -e -a -aes-128-cfb -in alex.txt -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A -out alex.cfb
@echo.
@pause

openssl enc -e -a -aes-128-ofb -in alex.txt -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A -out alex.ofb
@echo.
@pause

openssl enc -e -a -aes-128-ctr -in alex.txt -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A -out alex.ctr
@echo.
@pause

@rem jedit alex.ecb alex.cbc alex.cfb alex.ofb alex.ctr
@rem  edit alex.ecb alex.cbc alex.cfb alex.ofb alex.ctr

openssl enc -d -a -aes-128-ecb -in alex.ecb -K 6CB40CAEF6FDEDB31DBD908256F63276
@echo.
@pause

openssl enc -d -a -aes-128-cbc -in alex.cbc -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

openssl enc -d -a -aes-128-cfb -in alex.cfb -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

openssl enc -d -a -aes-128-ofb -in alex.ofb -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

openssl enc -d -a -aes-128-ctr -in alex.ctr -K 6CB40CAEF6FDEDB31DBD908256F63276 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

:END
@echo DONE