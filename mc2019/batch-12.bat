echo OFF
cls
echo ####################################################
echo Listagem 5.12. Certificados digitais para chaves RSA
echo ####################################################
echo.
echo ON

openssl genrsa -aes-128-cbc -out CA_chaves.rsa
@echo.
@pause

openssl req -new -key CA_chaves.rsa -x509 -out CA_raiz.crt
@echo.
@pause

openssl genrsa -aes-128-cbc -out alex.rsa
@echo.
@pause

openssl req -new -key alex.rsa -out alex.csr
@echo.
@pause

openssl x509 -req -in alex.csr -CA CA_raiz.crt -CAkey CA_chaves.rsa -out alex.crt -set_serial 3
@echo.
@pause

openssl verify -CAfile CA_raiz.crt CA_raiz.crt
@echo.
@pause

openssl verify -CAfile CA_raiz.crt alex.crt
@echo.


:END
@echo DONE