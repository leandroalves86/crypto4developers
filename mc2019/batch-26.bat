echo OFF
cls
echo #################################################################
echo Listagem 5.26. Exemplos de suites criptográficas nas versões 1.2 e 1.3 do TLS.
echo #################################################################
echo.
echo ON

openssl ciphers -v "TLSv1.2" | find "ECDHE" | find "AES"
@echo.
@pause

openssl ciphers -v "TLSv1.3"
@echo.
@pause
 
openssl ciphers -tls1_3
@echo.
@pause

:END
@echo DONE