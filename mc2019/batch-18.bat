echo OFF
cls
echo ##############################################
echo Listagem 5.18. Testes por suites criptograficas específicas
echo ##############################################
echo.
@echo ON

IF "%1" == "" goto END 


openssl ciphers -v "ALL" | find "TLSv1 " | find "ECDH"
@echo.
@pause

@echo.
@set command=openssl s_client -connect %1:443 -cipher ECDHE-ECDSA-AES256-SHA
@echo %command%
@echo.
@pause
@%command%
@echo.


:END
@echo DONE