echo OFF
cls
echo #######################################
echo Listagem 5.16. O OpenSSL como um cliente TLS
echo #######################################
echo.
echo ON

IF "%1" == "" goto END

openssl s_client -connect %1:443 -tls1_2
@echo.
@pause

openssl  ciphers -s 
@echo.
@pause

openssl s_client -connect %1 -port 443 -cipher AES128-SHA256
@echo.
@pause

openssl s_client -connect %1 -port 443 -cipher kECDHE
@echo.

:END
@echo DONE