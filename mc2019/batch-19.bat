echo OFF
cls
echo #############################################
echo Listagem 5.19. Teste de reuso de conex�es
echo #############################################
echo.
echo ON

IF "%1" == "" goto END 

openssl  s_client  -connect %1:443 -reconnect | find /I "Reused"
@echo.

:END
@echo DONE