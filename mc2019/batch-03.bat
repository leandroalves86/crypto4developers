echo OFF
cls
echo ###############################################
echo Listagem 5.3. Codificação em Base64 com OpenSSL
echo ###############################################
echo.
echo ON

type gabarito.txt
@echo.
@echo.
@pause

openssl enc -base64 -in gabarito.txt -out gabarito.b64 
@echo.
@pause

type gabarito.b64
@echo.
@echo.
@pause

openssl enc -d -base64 -in gabarito.b64 -out gabarito2.txt 
@echo.
@pause

type gabarito2.txt
@echo.
@echo.

:END
@echo DONE