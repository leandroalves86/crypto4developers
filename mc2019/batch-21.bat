echo OFF
cls
echo ###########################################################
echoListagem 5.21. Teste de grampeamento de OCSP (OCSP Stapling)
echo ###########################################################
echo.
echo ON

IF "%1" == "" goto END 

openssl s_client -connect %1:443 -status
@echo.
@pause

:END
@echo DONE