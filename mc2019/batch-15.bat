echo OFF
cls
echo ##########################################################################
echo Listagem 5.15. Assinaturas digitais e verificação de assinaturas com ECDSA
echo ##########################################################################
echo.
echo ON

openssl ecparam -name secp521r1 -out ecparam1.pem
openssl ecparam -name prime256v1 -out ecparam2.pem
@echo.
@pause

openssl ecparam -genkey -in ecparam1.pem -noout -out eckeys1.pem
openssl ecparam -genkey -in ecparam2.pem -noout -out eckeys2.pem 
@echo.
@pause


openssl ec -in eckeys1.pem -pubout -out public1.pem
openssl ec -in eckeys2.pem -pubout -out public2.pem
@echo.
@pause


openssl dgst -SHA512 -sign eckeys1.pem -out signature1.sign gabarito.txt
openssl dgst -SHA512 -sign eckeys2.pem -out signature2.sign gabarito.txt
@echo.
@pause

openssl dgst -SHA512 -verify public1.pem -signature signature1.sign gabarito.txt
@echo.
@pause

openssl dgst -SHA512 -verify public2.pem -signature signature2.sign gabarito.txt
@echo.
@pause

openssl ecparam -in ecparam2.pem -text -noout
@echo.
@pause

openssl ecparam -in ecparam2.pem -text -param_enc explicit -noout
@echo.
@pause

type signature2.sign
@echo.

:END
@echo DONE

