echo OFF
cls
echo ##################################################
echo Listagem 5.4. Gera��o de n�meros pseudo-aleat�rios
echo ##################################################
echo.
echo ON

openssl rand -hex 20
@echo.
@pause

openssl rand -hex 32 
@echo.
@pause

openssl rand -hex 64
@echo.
@pause

openssl rand -base64 64 
@echo.
@pause

openssl rand -base64 -out random.txt 64
@echo.
@pause

type random.txt
@echo.
@pause

openssl rand -base64 -rand random.txt 64

:END
@echo.
@echo DONE